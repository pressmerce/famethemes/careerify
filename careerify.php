<?php
/*
Plugin Name: Careerify
Plugin URI: http://famethemes.com/
Description:
Version: 1.0.0
Author: famethemes
Author URI: http://famethemes.com/
License: GPLv2 or later
Text Domain: careerify
*/

define( 'CAREERIFY_URL',  untrailingslashit( plugins_url('', __FILE__) ));
define( 'CAREERIFY_PATH', untrailingslashit( plugin_dir_path( __FILE__) ) );


// Gooogle sheet key  AIzaSyCc5_1vmw7gN4T7w8L6Xo04-ZroO1tz5N4
// https://developers.google.com/apis-explorer/?hl=en_US#p/sheets/v4/sheets.spreadsheets.values.get?spreadsheetId=1N9lBj5EOFP1rrZsYWvwvwv4dfMELey7ydPTGEEzUUh8&range=A2%253AR&_h=6&
// https://sheets.googleapis.com/v4/spreadsheets/1N9lBj5EOFP1rrZsYWvwvwv4dfMELey7ydPTGEEzUUh8/values/A2%3AR?key=AIzaSyCc5_1vmw7gN4T7w8L6Xo04-ZroO1tz5N4

require_once dirname( __FILE__ ).'/inc/functions.php';
require_once dirname( __FILE__ ).'/inc/class.install.php';
require_once dirname( __FILE__ ).'/inc/class.address.php';
require_once dirname( __FILE__ ).'/inc/class.post-type.php';
require_once dirname( __FILE__ ).'/inc/class.google-sheets.php';
require_once dirname( __FILE__ ).'/inc/class.career-schools-table.php';
require_once dirname( __FILE__ ).'/inc/class.career-salary-cites-table.php';
require_once dirname( __FILE__ ).'/inc/class.career-salary-states-table.php';
require_once dirname( __FILE__ ).'/inc/class.metabox.php';
require_once dirname( __FILE__ ).'/inc/class.career-schools.php';
require_once dirname( __FILE__ ).'/inc/class.career-salary-cites.php';
require_once dirname( __FILE__ ).'/inc/class.career-salary-states.php';
require_once dirname( __FILE__ ).'/inc/class.shortcode-us-map.php';
require_once dirname( __FILE__ ).'/inc/class.shortcode-charts.php';
require_once dirname( __FILE__ ).'/inc/class.editor-buttons.php';

require_once dirname( __FILE__ ).'/inc/class.ajax.php';
require_once dirname( __FILE__ ).'/inc/class.assets.php';
// Create data tables
register_activation_hook( __FILE__, 'careerify_install' );





// ==================for debug===============================
if(!function_exists('s_help_screen_help')){
    add_action( 'contextual_help', 's_help_screen_help', 10, 3 );
    function s_help_screen_help( $contextual_help, $screen_id, $screen ) {
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return ;
        }
// The add_help_tab function for screen was introduced in WordPress 3.3.
        if ( ! method_exists( $screen, 'add_help_tab' ) )
            return $contextual_help;
        global $hook_suffix;
        if ( ! isset( $hook_suffix ) ) {
           return $hook_suffix;
        }
// List screen properties
        $variables = '<ul style="width:50%;float:left;"> <strong>Screen variables </strong>'
            . sprintf( '<li> Screen id : %s</li>', $screen_id )
            . sprintf( '<li> Screen base : %s</li>', $screen->base )
            . sprintf( '<li>Parent base : %s</li>', $screen->parent_base )
            . sprintf( '<li> Parent file : %s</li>', $screen->parent_file )
            . sprintf( '<li> Hook suffix : %s</li>', $hook_suffix )
            . '</ul>';
// Append global $hook_suffix to the hook stems
        $hooks = array(
            "load-$hook_suffix",
            "admin_print_styles-$hook_suffix",
            "admin_print_scripts-$hook_suffix",
            "admin_head-$hook_suffix",
            "admin_footer-$hook_suffix"
        );
// If add_meta_boxes or add_meta_boxes_{screen_id} is used, list these too
        if ( did_action( 'add_meta_boxes_' . $screen_id ) )
            $hooks[] = 'add_meta_boxes_' . $screen_id;
        if ( did_action( 'add_meta_boxes' ) )
            $hooks[] = 'add_meta_boxes';
// Get List HTML for the hooks
        $hooks = '<ul style="width:50%;float:left;"> <strong>Hooks </strong> <li>' . implode( '</li><li>', $hooks ) . '</li></ul>';
// Combine $variables list with $hooks list.
        $help_content = $variables . $hooks;
// Add help panel
        $screen->add_help_tab( array(
            'id'      => 'wptuts-screen-help',
            'title'   => 'Screen Information',
            'content' => $help_content,
        ));
        return $contextual_help;
    }
}
/// ------------------------------
