<?php

class Careerify_Shortcode_Charts {

    function scripts( $career_id =  false,  $data = array() ){

        wp_register_script('career_chart', CAREERIFY_URL. '/assets/js/Chart.min.js', array( 'jquery' ), '1.0.0', true );
        wp_register_script('career_frontend', CAREERIFY_URL. '/assets/js/frontend.js', array( 'jquery' ), '1.0.0', true );

        wp_enqueue_script('underscore');
        wp_enqueue_script('backbone');
        wp_enqueue_script('career_chart');
        wp_enqueue_script('career_frontend');
        wp_localize_script( 'career_frontend', 'careerify', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
        ) );
        wp_localize_script( 'career_frontend', 'us_states', Careerify_Address::$states );

    }

    function compare_cities_scripts( $career_id =  false,  $data = array() ){
        wp_localize_script( 'career_frontend', 'career_chart_settings', array(
            'loading' => esc_html__( 'Loading...', 'careerify' ),
            'chart_keys' => array(
                'annual_10th_percentile_wage' => esc_html__( '10th percentile wage', 'careerify' ),
                'annual_25th_percentile_wage' => esc_html__( '25th percentile wage', 'careerify' ),
                'annual_75th_percentile_wage' => esc_html__( '75th percentile wage', 'careerify' ),
                'annual_90th_percentile_wage' => esc_html__( '90th percentile wage', 'careerify' ),
            )
        ) );
        add_action( 'wp_footer', array( $this, 'compare_cities_templates' ) );
    }

    function compare_state_scripts( $career_id =  false,  $data = array() ){
        wp_localize_script( 'career_frontend', 'career_chart_state_settings', array(
            'data' => $data,
            'loading' => esc_html__( 'Loading...', 'careerify' ),
            'chart_keys' => array(
                'annual_10th_percentile_wage' => esc_html__( '10th percentile wage', 'careerify' ),
                'annual_25th_percentile_wage' => esc_html__( '25th percentile wage', 'careerify' ),
                'annual_75th_percentile_wage' => esc_html__( '75th percentile wage', 'careerify' ),
                'annual_90th_percentile_wage' => esc_html__( '90th percentile wage', 'careerify' ),
            )
        ) );
        add_action( 'wp_footer', array( $this, 'compare_state_templates' ) );
    }

    function compare_cities_templates(){
        ?>
        <script id="career-compare-chart-tpl" type="text/html">
            <div class="compare-chart chart-id-{{ data.career_id }}">
                <div class="compare-chart-header">
                    <# if ( data.states ) { #>
                    <div class="states">
                        <select class="state-code" data-id="{{ data.career_id }}" name="state">
                            <option value=""><?php esc_html_e( 'Select a sate', 'careerify' ); ?></option>
                            <# _.each( data.states , function( value, key ){ #>
                            <option <# if ( data.selected_state == key ) { #> selected="selected" <# } #> value="{{ key }}">{{ value }}</option>
                            <# }); #>
                        </select>
                    </div>
                    <# } #>

                    <# if ( ! data.number_city || data.number_city < 1 ){ data.number_city = 3 };  #>
                    <# for ( var i = 0; i < data.number_city; i++  ) { #>
                        <# if ( data.cities ) { #>
                        <div class="cities">
                            <select class="compare-city" data-index="{{ i }}" name="city_{{ i }}">
                                <option value=""><?php esc_html_e( 'Select a city', 'careerify' ); ?></option>
                                <# _.each( data.cities , function( city, key ){ #>
                                    <option value="{{ city.area_id }}">{{ city.area_city }}</option>
                                <# }); #>
                            </select>
                        </div>
                        <# } #>
                    <# } //end loop city #>

                </div>
                <div class="compare-chart-content no-cities">
                    <div class="no-cities-note"><?php esc_html_e( 'Select cities to compare', 'careerify' ); ?></div>
                    <canvas class="compare-chart"></canvas>
                </div>
            </div>
        </script>

        <script id="compare-chart-info-tpl" type="text/html">
            <div class="compare-chart-info">
                <# _.each( data.cities, function( city, key ){ #>
                    <div class="compare-info-box">
                        <div class="info-box-inner">
                            <div class="label"><?php printf( esc_html__( '%s mean pay', 'careerify' ), '{{ city.area_city }}' ); ?></div>
                            <div class="value"><?php printf( esc_html__( '%s per year', 'careerify' ), '{{ city.annual_mean_wage }}' ); ?></div>
                            <div class="value"><?php printf( esc_html__( '%s per hour', 'careerify' ), '{{ city.hourly_mean_wage }}' ); ?></div>
                        </div>
                    </div>
                <# }); #>
            </div>
        </script>
        <?php
    }


    function compare_state_templates(){
        ?>
        <script id="career-compare-state-chart-tpl" type="text/html">
            <div class="compare-chart-state chart-id-{{ data.career_id }}">
                <div class="compare-chart-header">
                    <# if ( data.states ) { #>
                        <div class="states">
                            <select class="state-code" data-id="{{ data.career_id }}" name="state">
                                <option value=""><?php esc_html_e( 'Select a sate', 'careerify' ); ?></option>
                                <# _.each( data.states , function( value, key ){ #>
                                    <option <# if ( data.selected_state == key ) { #> selected="selected" <# } #> value="{{ key }}">{{ value }}</option>
                                <# }); #>
                            </select>
                        </div>
                    <# } #>
                </div>
                <div class="compare-chart-content no-states">
                    <div class="no-cities-note"><?php esc_html_e( 'Select state to compare', 'careerify' ); ?></div>
                    <canvas class="compare-chart"></canvas>
                </div>
            </div>
        </script>
        <?php
    }


    function add_scripts(){
        add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
    }
    function render_content_cities( $career_id = null ){
        return '<div data-id="'.esc_attr( $career_id ).'" class="compare-chart"></div>';
    }

    function render_content_state( $career_id = null, $state = '' ){
        return '<div data-id="'.esc_attr( $career_id ).'" default-state="'.strtoupper( esc_attr( $state ) ).'" class="compare-chart-state"></div>';
    }

    /**
     * Get note from sheet value
     *
     * @param $value
     * @param string $type
     * @return float|int
     */
    function get_note( $value, $type = 'year' ) {
        if ( false !== strpos( $value, '(5)' ) ) {
            if ( $type == 'year' ) {
                return 187200;
            } else {
                return 90.00;
            }
        } else if ( false !== strpos( $value, '(8)' ) ) {
            return esc_html__( 'N/A', 'careerify' );// .Estimate not released
        } else if ( is_numeric( $value ) ) {
            return $value;
        }
        return -2; // Unknown

    }

    static function shortcode_compare_cities( $atts, $content = false ){

        $atts = shortcode_atts( array(
            'career' => '',
            'id' => '',
        ), $atts );

        $career_id = 0;
        if( $atts['career'] ) {
            $post = get_page_by_path( untrailingslashit( $atts['career'] ) , OBJECT, 'career');
            if ( $post ) {
                $career_id = $post->ID;
            }
        }

        if ( ! $career_id && $atts[ 'id' ] ) {
            $career_id = $atts['id'];
        }

        if ( ! $career_id ) {
            global $post;
            $career_id = $post->ID;
        }

        if ( ! $career_id ) {
            return false;
        }

        $self = new self();

        $s = new Careerify_Career_Salary_Cites( $career_id );
        $data = $s->get_list_salary( array( 'per_page' => -1 ) );

        $array = array();
        $keys = array(
            'annual_mean_wage' => 'year',
            'annual_10th_percentile_wage' => 'year',
            'annual_25th_percentile_wage' => 'year',
            'annual_75th_percentile_wage' => 'year',
            'annual_90th_percentile_wage' => 'year',
        );

        $array = array();

        $self->scripts();
        $self->compare_cities_scripts( $career_id, $array  );

        return $self->render_content_cities( $career_id );
    }


    static function shortcode_compare_state( $atts, $content = false ){

        $atts = shortcode_atts( array(
            'career' => '',
            'id' => '',
            'state' => 'AL',
        ), $atts );

        $career_id = 0;
        if( $atts['career'] ) {
            $post = get_page_by_path( untrailingslashit( $atts['career'] ) , OBJECT, 'career');
            if ( $post ) {
                $career_id = $post->ID;
            }
        }

        if ( ! $career_id && $atts[ 'id' ] ) {
            $career_id = $atts['id'];
        }

        if ( ! $career_id ) {
            global $post;
            $career_id = $post->ID;
        }

        if ( ! $career_id ) {
            return false;
        }

        $self = new self();
        $array = array();

        $self->scripts();
        $self->compare_state_scripts( $career_id, $array  );
        return $self->render_content_state( $career_id, $atts['state'] );
    }
}

add_shortcode( 'careerify_compare_cities', array( 'Careerify_Shortcode_Charts', 'shortcode_compare_cities' ) );
add_shortcode( 'careerify_compare_state', array( 'Careerify_Shortcode_Charts', 'shortcode_compare_state' ) );