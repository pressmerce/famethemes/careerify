<?php
/**
 * Created by PhpStorm.
 * User: truongsa
 * Date: 10/31/16
 * Time: 8:43 AM
 */
class Careerify_Editor_Buttons {
    static $intance;
    static function maybe_init(){
        if ( ! self::$intance ) {
            self::$intance = new self();
        }
    }
    static function add_button(){
        self::maybe_init();
        wp_enqueue_script( 'career_editor', CAREERIFY_URL. '/assets/js/editor.js', array( 'jquery' ), '1.0.0', true );
        ?>
        <button type="button" id="insert-media-career" class="button insert-media-career" data-editor="content"><span class="dashicons dashicons-external"></span> Careerify</button>
        <?php
        add_action( 'admin_footer', array( self::$intance, 'modal_template' ) );
    }

    function get_careers(){
        global $wpdb;
        $sql = "SELECT ID, post_title, post_name FROM $wpdb->posts WHERE post_type = 'career' AND post_status = 'publish' ORDER BY post_title ASC ";
        return $wpdb->get_results( $sql );
    }
    function modal_template(){
        self::maybe_init();
        $careers = self::$intance->get_careers();

        ?>
        <div class="career-modal-shortcode hidden" tabindex="0">
            <div class="media-modal career-modal wp-core-ui">
                <button type="button" class="button-link media-modal-close">
                    <span class="media-modal-icon"><span class="screen-reader-text"><?php esc_html_e( 'Close media panel', 'careerify' ); ?></span></span>
                </button>
                <div class="media-modal-content">
                    <div class="media-frame mode-select wp-core-ui hide-menu hide-toolbar hide-router">

                        <div class="media-frame-title">
                            <h1><?php esc_html_e( 'Career shortcode', 'careerify' ); ?><span class="dashicons dashicons-arrow-down"></span></h1>
                        </div>

                        <div class="media-frame-content">
                            <div class="media-frame-content-inner">

                                <div class="shortcode-creator">

                                    <select class="shortcode-career">
                                        <option value=""><?php esc_html_e( 'Select career', 'careerify' ); ?></option>
                                        <?php
                                        if ( $careers ) {
                                            foreach ( $careers as $career ) {
                                                ?>
                                                <option value="<?php echo esc_attr( $career->post_name ); ?>"><?php esc_attr_e( $career->post_title ); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>

                                    <select class="shortcode-name">
                                        <option value=""><?php esc_html_e( 'Select Shortcode', 'careerify' ); ?></option>
                                        <?php
                                        $shortcode_codes =array(
                                            'careerify_school'          => esc_html__( 'Schools', 'careerify' ),
                                            'careerify_usmap'           => esc_html__( 'US Map', 'careerify' ),
                                            'careerify_compare_cities'  => esc_html__( 'Compare cities', 'careerify' ),
                                            'careerify_compare_state'   => esc_html__( 'Compare state', 'careerify' ),
                                        );
                                        foreach ( $shortcode_codes as $k => $v ) {
                                            ?>
                                            <option value="<?php echo esc_attr( $k ); ?>"><?php esc_attr_e( $v ); ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                    <select class="shortcode-state">
                                        <option value=""><?php esc_html_e( 'Select State', 'careerify' ); ?></option>
                                        <?php
                                        foreach ( Careerify_Address::$states as $k => $v ) {
                                            ?>
                                            <option value="<?php echo esc_attr( $k ); ?>"><?php esc_attr_e( $v ); ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </div>

                               <div class="shortcode-career-generate-w">
                                   <input type="text" readonly class="shortcode-career-generated">
                                   <button class="button insert-career-shortcode button-secondary"><?php esc_html_e( 'Insert shortcode', 'careerify' ); ?></button>
                               </div>

                            </div><!-- /.media-frame-content-inner -->
                        </div>

                        <div class="media-frame-toolbar">
                            <div class="media-toolbar">
                                <div class="media-toolbar-primary search-form">
                                    <button type="button" class="button media-button button-primary button-large media-button-select" disabled="disabled">Insert Shortcode</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="media-modal-backdrop shortcode-drop hidden"></div>
        </div>
        <?php
    }
}
add_action( 'media_buttons', array( 'Careerify_Editor_Buttons', 'add_button' ) );