<?php

class Careerify_Career_Salary_Cites {

    public $career_id;
    public $table;
    public $table_areas;

    private $salary_fields = array(
        'salary_id',
        'career_id',
        'area_id',
        'employment_number',
        'hourly_mean_wage',
        'annual_mean_wage',
        'annual_10th_percentile_wage',
        'annual_25th_percentile_wage',
        'annual_75th_percentile_wage',
        'annual_90th_percentile_wage',
    );

    private $area_fields = array(
        'area_id',
        'area_name',
        'area_state',
        'area_city',
    );

    function __construct( $career_id = 0 ) {
        $this->set_career_id( $career_id );
        global $table_prefix;
        $this->table = $table_prefix.'careerify_salary_cities';
        $this->table_areas = $table_prefix.'careerify_areas';
    }
    function set_career_id( $career_id ) {
        $this->career_id = absint( $career_id );
    }


    function count_career_salary( $career_id = 0){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }
        global $wpdb;
        $sql = "SELECT count( salary_id ) as found_items FROM $this->table
        WHERE career_id = %d";
        $r = $wpdb->get_var( $wpdb->prepare( $sql, $career_id ) );
        return $r;
    }

    function get_list_salary( $get_args = array() , $career_id = 0, $output = 'ARRAY_A' ){
        global $wpdb;
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        $get_args = wp_parse_args( $get_args,array(
            'per_page'  => 100,
            'paged'     => 1,
            'orderby'   => 'area_city',
            'order'     => 'ASC',
            'state'     => ''
        ) );
        extract( $get_args );


        if ( ! $order )  {
            $order = 'ASC';
        }

        if ( strtolower( $order ) != 'asc' ) {
            $order = 'desc';
        }
        if ( ! $orderby ) {
            $orderby = 'area_city';
        }

        if ( ! in_array( $orderby, $this->salary_fields ) ) {
            $orderby = 'area_city';
        }

        if ( $paged <= 0 ) {
            $paged = 1;
        }

        $where = '';
        if ( $state ) {
           $where = " AND ar.area_state LIKE '".$wpdb->_real_escape( $state )."' ";
        }

        $sql = "SELECT * FROM $this->table AS cs
        LEFT JOIN $this->table_areas AS ar ON cs.area_id = ar.area_id
        WHERE cs.career_id = %d $where
        ORDER BY $orderby $order
        ";

        $offset = ($paged - 1) * $per_page;
        if ( $per_page > 0 ) {
            $sql .=" LIMIT %d, %d ";
        }

        $sql = $wpdb->prepare( $sql, $career_id, $offset, $per_page );
        $r = $wpdb->get_results( $sql , $output );

        return $r;
    }

    function delete_career_salary( $career_id = null ){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        global $wpdb;
        $sql = "DELETE FROM $this->table WHERE career_id = %d ";
        $wpdb->query( $wpdb->prepare( $sql, $career_id ) );

    }


    function sync( $google_sheet_url, $force_career_id = null, $update_if_exists = true ) {
        $gs = new Careerify_Google_Sheets();
        $data = $gs->get_sheet_data( $google_sheet_url, 'A2:R' );

        if ( is_wp_error( $data ) ) {
            return false;
        }

        if ( ! is_array( $data ) ) {
            return false;
        }

        if ( ! isset( $data['values'] ) ) {
            $data['values'] = array();
        }

        if ( true === $force_career_id ) {
            $career_id = $this->career_id;
        } else {
            $career_id = $force_career_id;
        }

        $ids = array();

        $count = 0; // Count number schools imported
        foreach ( ( array ) $data['values'] as $k => $item ) {
            if ( count( $item ) < 15 ) {
                continue;
            }

            $area_name = $item[ 0 ];
            $area_data = Careerify_Address::parse_area_name( $area_name );
            if ( ! $area_data ) {
                continue;
            }

            $tpl_item = array(
                'salary_id' => '',
                'career_id' => $career_id,
                'area_id' => '',
                'employment_number' => $item[1],
                'hourly_mean_wage' => $item[3],
                'annual_mean_wage' => $item[4],
                'annual_10th_percentile_wage' => $item[11],
                'annual_25th_percentile_wage' => $item[12],
                'annual_75th_percentile_wage' => $item[14],
                'annual_90th_percentile_wage' => $item[15],
            );
            foreach ( $area_data['cities'] as $city ) {
                $id = $this->set_salary_for_area( $tpl_item, $city, $area_data['state_code'], $career_id, $k );
                if ( $id ) {
                    $count ++ ;
                    $ids[] = $id;
                }
            }

        } //End foreach $data['values']

        // Delete schools not in the sheet
        if ( ! empty( $ids ) ) {
            $ids_not_in = 'NOT IN('.join( ',', $ids ).')';
            global $wpdb;

            $sql = "DELETE FROM $this->table WHERE salary_id $ids_not_in AND career_id = %d ";
            $wpdb->query( $wpdb->prepare( $sql, $career_id ) );

        }
        return $count;

    }

    function get_area( $city_name, $state_code, $output = ARRAY_A ){
        global $wpdb;
        $sql = "SELECT * FROM $this->table_areas WHERE area_city LIKE %s AND area_state LIKE %s";
        $r = $wpdb->get_row( $wpdb->prepare( $sql, $city_name, $state_code ), $output );
        return $r;
    }
    function insert_area( $city_name , $state_code, $item_order = 0 ) {

        $r = $this->get_area( $city_name, $state_code );
        if ( $r ) {
            return $r->area_id;
        }
        global $wpdb;
        $r = $wpdb->insert (
            $this->table_areas,
            array(
                'area_city'  => $city_name,
                'area_state' => $state_code,
                'item_order' => $item_order,
            )
        );
        $id = false;
        if ( $r ) {
            $id = $wpdb->insert_id;
        }
        return $id;
    }

    function update_area( $area_id, $data ){
        global $wpdb;
        return $wpdb->update(
            $this->table_areas,
            $data,
            array( 'area_id' => $area_id )
        );
    }

    function get_salary_by_area( $city, $state, $career_id = null ) {
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        global $wpdb;
        $sql =  "SELECT * FROM {$this->table} as t
          LEFT JOIN $this->table_areas as ta ON t.area_id = ta.area_id
          WHERE ta.area_city = %s AND ta.area_state = %s AND t.career_id = %d";
        $row = $wpdb->get_row( $wpdb->prepare( $sql , $city, $state, $career_id ) , ARRAY_A );
        return $row;
    }

    function set_salary_for_area( $salary_data = array(), $city, $state, $career_id = null, $area_order = 0 ){
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        if ( ! $career_id ) {
            return false;
        }

        global $wpdb;

        $area = $this->get_area( $city, $state );

        if ( ! $area ) {
            $area_id = $this->insert_area( $city, $state, $area_order );
        } else {
            $this->update_area( $area['area_id'], array(
                'item_order' => $area_order,
            ) );
            $row = $this->get_salary_by_area( $city, $state, $career_id ); // Check if salary row already import
            if ( ! $row ) {
                $area_id = $area['area_id'];
            } else {
                return $row['salary_id'];
            }

        }

        if ( ! $area_id ) {
            return false;
        }

        $salary_data[ 'area_id' ]   = $area_id;
        $salary_data[ 'career_id' ] = $career_id;
        $r = $wpdb->insert ( $this->table, $salary_data );
        if ( $r ) {
            $id = $wpdb->insert_id;
        } else {
            $id = false;
        }
        return $id;

    }

}