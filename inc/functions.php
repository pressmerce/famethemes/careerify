<?php
/**
 * Get note from sheet value
 *
 * @param $value
 * @param string $type
 * @return float|int
 */
function careerify_get_salary_note( $value, $type = 'year' ) {
    if ( false !== strpos( $value, '(5)' ) ) {
        if ( $type == 'year' ) {
            return 187200;
        } else {
            return 90.00;
        }
    } else if ( false !== strpos( $value, '(8)' ) ) {
        return esc_html__( 'N/A', 'careerify' );// .Estimate not released
    } else if ( is_numeric( $value ) ) {
        return $value;
    }
    return -2; // Unknown

}