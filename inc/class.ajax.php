<?php
class Careerify_Ajax {
    function __construct() {
        add_action( 'wp_ajax_careerify_admin_ajax', array( $this, 'admin' ) );
        add_action( 'wp_ajax_nopriv_careerify_admin_ajax', array( $this, 'admin' )  );


        add_action( 'wp_ajax_careerify_frontend_ajax', array( $this, 'frontend' ) );
        add_action( 'wp_ajax_nopriv_careerify_frontend_ajax', array( $this, 'frontend' )  );
    }

    function frontend(){
        $doing = isset( $_REQUEST['doing'] ) ? $_REQUEST['doing'] : false;
        switch ( $doing ) {
            case 'get_salary_state':
                $state = isset( $_REQUEST['state'] ) ? $_REQUEST['state'] : false;
                $career_id = isset( $_REQUEST['career_id'] ) ? $_REQUEST['career_id'] : false;
                $cs = new Careerify_Career_Salary_Cites( $career_id );
                $rows = $cs->get_list_salary( array(
                    'state' => $state,
                    'per_page' => -1,
                    'orderby' => 'ar.item_order',
                    'order' => 'ASC',
                ) );

                $arr = array();
                $keys = array(
                    'annual_mean_wage' => 'year',
                    'annual_10th_percentile_wage' => 'year',
                    'annual_25th_percentile_wage' => 'year',
                    'annual_75th_percentile_wage' => 'year',
                    'annual_90th_percentile_wage' => 'year',
                );
                foreach ( $rows as $item ){
                    foreach ( $keys as $k => $type ) {
                        if ( isset( $item[ $k ] ) ) {
                            $t = careerify_get_salary_note( $item[ $k ], $type );
                            if ( $t > -1 ) {
                                $item[ $k ] = $t;
                            }
                        }
                    }
                    $arr[ $item['area_id'] ] = $item;
                }
                wp_send_json_success( $arr );
                break;
        }
        die();
    }

    function admin(){
        if ( ! is_admin() ) {
            wp_die( 'Security check' );
        }
        //$school = new Careerify_Career_Schools( 0 );
        //$school->sync( 'https://docs.google.com/spreadsheets/d/1Mye8dU3Xm1WBXToBw-MOiDm8t8AcBlmTg24uDXet9J8/edit#gid=0' );

        $doing = isset( $_REQUEST['doing'] ) ? $_REQUEST['doing'] : false;
        $nonce = isset( $_REQUEST['nonce'] ) ? $_REQUEST['nonce'] : '';
        if ( ! wp_verify_nonce( $nonce, 'career_admin' ) ) {
            wp_die( 'Security check' );
        }
        switch ( $doing ) {
            case 'get_html':
                $url = $_REQUEST['url'];
                $r = wp_remote_get( $url );
                if ( wp_remote_retrieve_response_code( $r ) == 200 ) {
                    echo wp_remote_retrieve_body( $r );
                } else {
                    echo '';
                }

                die();
                break;
            case 'get_schools':
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                Carreerify_Meta_Box::modal_schools( $career_id );
                break;
            case 'get_salary_cities':
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                Carreerify_Meta_Box::modal_salary_cities( $career_id );
                break;
            case 'get_salary_states':
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                Carreerify_Meta_Box::modal_salary_states( $career_id );
                break;

            case 'insert_program':
                $data       = $_POST['data'];
                $dg         = $_POST['dg'];
                $pr         = $_POST['pr'];
                $career_id  = $_POST['career_id'];
                if ( $dg[0] )

                $program_data = array(
                    'program_name' => trim( $pr[0] ),
                    'program_slug' => sanitize_title( $pr[0] ),
                    'career_id' => $career_id,
                    'school_id' => $data['school_id'],
                );

                $degree_levels = array();
                for ( $i = 1; $i < count( $pr ) ; $i++ ) {
                    if ( isset( $dg[ $i ] ) ){
                        $v = $pr[ $i ];
                        if ( $v !== '-' ){
                            $key = $dg[ $i ];
                            $key = str_replace(array( '<br/>', '<br />', '<br>') , ' ', $key );
                            $degree_levels[ $key ] = $v;
                        }
                    }
                }

                $cs = new Careerify_Career_Schools( $career_id );
                foreach ( $degree_levels as $k => $v ) {
                    $pr_item = $program_data;
                    $pr_item['degree_level']    = $k;
                    $pr_item['level_value']     = $v;
                    $cs->set_program_school( $pr_item );
                }


                break;
            case 'sync_schools':

                //$startTime = microtime(true);
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                // $google_sheet_url = isset( $_REQUEST['google_sheer_url'] ) ? esc_url( $_REQUEST['google_sheer_url'] ) : '';
                // Update sheet URL
                //update_post_meta( $career_id, '_careerify_school_google_sheet_url', $google_sheet_url );

                $program_name = isset( $_REQUEST['program'] ) ?  trim( $_REQUEST['program'] ) : false;
                $sheet = isset( $_REQUEST['sheet'] ) ?  $_REQUEST['sheet'] : false;
                $program_slug = sanitize_title( $program_name );
                $cs = new Careerify_Career_Schools( $career_id );

                $sheets = get_post_meta( $career_id, '_careerify_school_sheets', true );
                if ( ! is_array( $sheets ) ) {
                    $sheets = array();
                }

                $programs = isset( $_REQUEST['programs'] ) ? $_REQUEST['programs'] : false;
                if ( is_array( $programs ) ) {
                    // Remove all program not in the list
                    $programs = array_map( "sanitize_title", $programs);
                    $programs = array_flip( $programs );
                    foreach ( $sheets as $k => $v ) {
                        if ( ! isset( $programs[ $k ] ) ) {
                            $cs->remove_program( $k );
                        }
                    }
                }

                if ( $program_slug ) {
                    $sheets[ $program_slug ] = array(
                        'name' => $program_name,
                        'url' => $sheet
                    );
                }

                update_post_meta( $career_id, '_careerify_school_sheets', $sheets );


                $data = array(
                    'sheet'         => $sheet,
                    'program_name'  => $program_name,
                    'program_slug'  => $program_slug,
                );
                $schools_ids  = $cs->sync( $data, true );

                $r = array();
                //$r['string'] = sprintf( _n( '%d School', '%d Schools', $_count, 'careerify' ), $_count );
                $r['schools_ids'] = $schools_ids;
                $r['career_id'] = $career_id;
                //$r['program_name'] = $program_name;
                //$r['program_slug'] = $program_slug;
                wp_send_json_success( $r );
                break;

            case 'remove_school_program':
                $program_name =  $_REQUEST['program'];

                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                $cs = new Careerify_Career_Schools( $career_id );
                $cs->remove_program( $program_name );

                break;

            case 'sync_salary_cities':
                //$startTime = microtime(true);
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                $google_sheet_url = isset( $_REQUEST['google_sheer_url'] ) ? esc_url( $_REQUEST['google_sheer_url'] ) : '';
                // Update sheet URL
                update_post_meta( $career_id, '_careerify_salary_cities_google_sheet_url', $google_sheet_url );
                $cs = new Careerify_Career_Salary_Cites( $career_id );
                $count  = $cs->sync( $google_sheet_url, true );
                // echo "Time:  " . number_format(( microtime(true) - $startTime), 4) . " Seconds\n";
                echo $count;
                break;

            case 'sync_salary_states':
                //$startTime = microtime(true);
                $career_id = isset( $_REQUEST['post_id'] ) ? absint( $_REQUEST['post_id'] ) : 0;
                $google_sheet_url = isset( $_REQUEST['google_sheer_url'] ) ? esc_url( $_REQUEST['google_sheer_url'] ) : '';
                // Update sheet URL
                update_post_meta( $career_id, '_careerify_salary_states_google_sheet_url', $google_sheet_url );
                $cs = new Careerify_Career_Salary_States( $career_id );
                $count  = $cs->sync( $google_sheet_url, true );
                // echo "Time:  " . number_format(( microtime(true) - $startTime), 4) . " Seconds\n";
                echo $count;
                break;
        }

        die( );
    }
}

$GLOBALS['Careerify_Ajax'] = new Careerify_Ajax();