<?php
class Careerify_Install {

    function __construct() {
        $this->create_tables();
    }

    function create_tables(){
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $prefix = $wpdb->prefix;

        $tables = "
CREATE TABLE {$prefix}careerify_schools (
  `school_id` bigint(20) unsigned AUTO_INCREMENT,
  `school_name` VARCHAR(255) NOT NULL default '',
  `school_type` VARCHAR(100) NOT NULL default '',
  `school_address` VARCHAR(255) NOT NULL default '',
  `school_state` VARCHAR(50) NOT NULL default '',
  `school_website` VARCHAR(255) NOT NULL default '',
  `school_phone` VARCHAR(255) NOT NULL default '',
  `school_email` VARCHAR(255) NOT NULL default '',
  `school_awards_offered` TEXT NOT NULL default '',
  `IPEDS_ID` TEXT NOT NULL default '',
  PRIMARY KEY  (school_id)
) $charset_collate ;

CREATE TABLE {$prefix}careerify_schools_programs (
  program_id bigint(20) unsigned AUTO_INCREMENT,
  school_id bigint(20) unsigned NOT NULL default 0,
  career_id bigint(20) unsigned NOT NULL default 0,
  program_slug VARCHAR(255) NOT NULL default '',
  program_name VARCHAR(255) NOT NULL default '',
  degree_level VARCHAR(50) NOT NULL default '',
  level_value VARCHAR(50) NOT NULL default '',
  PRIMARY KEY ( program_id ),
  KEY career_id (career_id)
) $charset_collate;

CREATE TABLE {$prefix}careerify_salary_cities (
  salary_id bigint(20) unsigned AUTO_INCREMENT,
  career_id bigint(20) NOT NULL default 0,
  area_id int(15) NOT NULL default 0,
  employment_number int(11) NOT NULL default 0,
  hourly_mean_wage int(11) NOT NULL default 0,
  annual_mean_wage int(11) NOT NULL default 0,
  annual_10th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_25th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_75th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_90th_percentile_wage VARCHAR(15) NOT NULL default 0,
  PRIMARY KEY  (salary_id),
  KEY career_id (career_id),
  KEY area_id (area_id)
) $charset_collate ;

CREATE TABLE {$prefix}careerify_salary_states (
  csd_id bigint(20) unsigned AUTO_INCREMENT,
  career_id bigint(20) NOT NULL default 0,
  state_code VARCHAR (10) NOT NULL default 0,
  employment_number int(11) NOT NULL default 0,
  hourly_mean_wage int(11) NOT NULL default 0,
  annual_mean_wage int(11) NOT NULL default 0,
  annual_10th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_25th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_75th_percentile_wage VARCHAR(15) NOT NULL default 0,
  annual_90th_percentile_wage VARCHAR(15) NOT NULL default 0,
  PRIMARY KEY  (csd_id),
  KEY career_id (career_id ),
  KEY state_code (state_code)
) $charset_collate ;

CREATE TABLE  {$prefix}careerify_areas (
  area_id bigint(20) unsigned AUTO_INCREMENT,
  `area_state` VARCHAR(60) NOT NULL default '',
  `area_city` VARCHAR(100) NOT NULL default '',
  `item_order` int(11) NOT NULL default 0,
  PRIMARY KEY  (area_id)
) $charset_collate ; ";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $tables );

    }

}

function careerify_install(){
    $install = new Careerify_Install();
}