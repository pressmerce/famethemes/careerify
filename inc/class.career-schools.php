<?php

class Careerify_Career_Schools {

    public $career_id;
    public $table;
    public $table_relationships;

    private $sheet_fields = array(
        'name' => 0,
        'school_address' => 1,
        'website' => 2,
        'type' => 3,
        'awards_offered' => 4,
        'IPEDS_ID' => 14,
        'OPE_ID' => 15,
    );

    private $relationship_fields = array(
        'degree_level' => 2,
        'programs_offered' => 3,
        //'number_of_awards' => false,
    );

    private $school_fields = array(
        'school_name' => 'name',
        'school_type' => 1,
        'school_address' => 4,
        'school_state' => 6,
        'school_website' => 7,
        //'school_phone' => 8,
        //'school_email' => 9,
        'school_overview' => 10,
    );

    function __construct( $career_id = 0 ) {
        $this->set_career_id( $career_id );
        global $table_prefix;
        $this->table = $table_prefix.'careerify_schools';
        $this->table_relationships = $table_prefix.'careerify_schools_programs';
    }
    function set_career_id( $career_id ) {
        $this->career_id = absint( $career_id );
    }

    function remove_program( $program_name ) {
        $program_name = sanitize_title( $program_name );
        if ( ! $program_name ) {
            return ;
        }
        global $wpdb;
        $career_id = $this->career_id;
        $sql = "DELETE FROM $this->table_relationships WHERE career_id = %d AND program_slug = %s";
        $wpdb->query( $wpdb->prepare( $sql, $career_id, $program_name ) );

        $sheets = get_post_meta( $career_id, '_careerify_school_sheets', true );
        if ( ! is_array( $sheets ) ) {
            $sheets = array();
        }
        if ( $sheets[ $program_name ] ) {
            unset( $sheets[ $program_name ] );
            update_post_meta( $career_id, '_careerify_school_sheets', $sheets );
        }
    }

    function count_schools( $career_id = 0 ){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }
        global $wpdb;
        $sql = "SELECT count( sc.school_id ) as found_items FROM $this->table AS sc
        LEFT JOIN $this->table_relationships AS scc ON sc.school_id = scc.school_id
        WHERE scc.career_id = %d ORDER BY school_name ASC";

        $r = $wpdb->get_var( $wpdb->prepare( $sql, $career_id ) );
        return $r;
    }

    function get_schools( $get_args = array() , $career_id = 0, $output = 'ARRAY_A' ){
        global $wpdb;
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        $get_args = wp_parse_args( $get_args,array(
            'per_page'  => 100,
            'paged'     => 1,
            'orderby'   => 'school_name',
            'order'     => 'ASC'
        ) );
        extract( $get_args );

        if ( ! $order )  {
            $order = 'ASC';
        }

        if ( strtolower( $order ) != 'asc' ) {
            $order = 'desc';
        }
        if ( ! $orderby ) {
            $orderby = 'school_name';
        }

        if ( $orderby == 'school_career' || ! in_array( $orderby, $this->school_fields ) ) {
            $orderby = 'school_name';
        }

        if ( $paged <= 0 ) {
            $paged = 1;
        }
        $offset = ($paged - 1) * $per_page;
        $sql = "SELECT * FROM $this->table_relationships  AS scc
        LEFT JOIN $this->table AS sc ON sc.school_id = scc.school_id
        WHERE scc.career_id = %d
        ORDER BY $orderby $order
        LIMIT %d, %d";

        $r = $wpdb->get_results( $wpdb->prepare( $sql, $career_id, $offset, $per_page ), $output );
        return $r;
    }

    function delete_career_schools( $career_id = null ){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        global $wpdb;
        $sql = "DELETE FROM $this->table WHERE school_id IN ( SELECT school_id FROM $this->table_relationships WHERE career_id = %d )";
        $wpdb->query( $wpdb->prepare( $sql, $career_id ) );


        // Delete from table_relationships
        $sql = "DELETE FROM $this->table_relationships WHERE career_id = %d";
        $wpdb->query( $wpdb->prepare( $sql, $career_id ) );

    }

    function get_state_from_address( $address ){
        $address =  preg_replace('/[0-9]+/', '', $address );
        $address = str_replace( '-', '', trim( $address ) ) ;
        $address = explode( ',', $address );
        $state = trim( end( $address ) );
        return $state;
    }

    function get_url_from_string( $text ){
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if( preg_match($reg_exUrl, $text, $url)) {
            return $url[0];
        } else {
            return false;
        }
    }


    function sync( $args = array(), $force_career_id = null, $update_if_exists = true ) {

        $args = wp_parse_args( $args, array(
            'sheet'         => '',
            'program_name'  => '',
            'program_slug'  => '',
        ) );

        if ( ! $args['sheet'] ) {
            return false;
        }

        $gs = new Careerify_Google_Sheets();
        $data = $gs->get_sheet_data( $args['sheet'], 'A3:P' );
        if ( is_wp_error( $data ) ) {
            return false;
        }

        if ( ! is_array( $data ) ) {
            return false;
        }

        if ( ! isset( $data['values'] ) ) {
            $data['values'] = array();
        }

        $url = $this->get_url_from_string( $data['values'][0][0] );
        $program_id = 0;
        if ( $url ) {
            $parse_url = wp_parse_url( $url ) ;
            $url_data = wp_parse_args( $parse_url['query'] );
            $program_id = $url_data['p'];
        }

        if ( true === $force_career_id ) {
            $career_id = $this->career_id;
        } else {
            $career_id = $force_career_id;
        }

        $schools = array();
        $schools_data = array();
        foreach ( ( array ) $data['values'] as $k => $item ) {
            if ( count( $item ) < 10 ) {
                continue;
            }

            if ( $item[ 0 ] == 'Name' ) {
                continue;
            }

            $school_item = array();

            $school_item['school_name'] = $item[ 0 ];
            $school_item['school_address'] = $item[ 1 ];
            $school_item['school_type'] = $item[ 3 ];
            $school_item['school_website'] = $item[ 2 ];
            $school_item['school_state'] = Careerify_Address::get_state_code( $this->get_state_from_address( $school_item['school_address'] ) );
            $school_item['school_awards_offered'] = $item[ 4 ];
            $school_item['IPEDS_ID'] = $item[ 14 ];

            /*
            $program_item = array();
            $program_item['program_nces_id'] = $item[ 14 ].'|'.$program_id;
            $program_item['career_id'] = $career_id;
            $program_item['school_id'] = '';
            $program_item['program_name'] = $args['program_name'];
            $program_item['program_slug'] = $args['program_slug'];
            */

            if ( ! empty( $school_item ) && isset ( $school_item['school_name'] ) ) {

                if ( $update_if_exists ) {
                    $check = $this->get_school_by_name(  $school_item['school_name'] );
                    if ( $check && isset( $check['school_id'] ) ) {
                        $this->update_school( $check['school_id'], array_filter( $school_item ) );
                        $school_id = $check['school_id'];
                    } else {
                        $school_id = $this->insert_school( $school_item );
                    }
                } else {
                    $school_id = $this->insert_school( $school_item );
                }

                if ( $school_id ) {
                    $schools[ $school_id ] = $school_id;
                    $schools_data[ ] = array(
                        'IPEDS_ID'      => $school_item['IPEDS_ID'],
                        'program_id'    => $program_id,
                        'school_id'     => $school_id,
                    );

                    /*
                    $count++;
                    $p_id = false;
                    $program_item[ 'school_id' ] = $school_id;
                    if ($force_career_id) { // Force import for current career only.
                        $p_id = $this->set_program_school( $program_item, $career_id );
                    } else {
                        $career = isset( $school_item['career_name'] ) ? $school_item['career_name'] : false;
                        if ( $career ) {
                            $post = get_page_by_title($career, OBJECT, 'career');
                            if ($post) {
                                $p_id = $this->set_program_school( $program_item, $post->ID);
                            } else { // insert career
                                $post_id = wp_insert_post(array(
                                    'post_title' => $career,
                                    'post_content' => '',
                                    'post_status' => 'publish',
                                    'post_type' => 'career',
                                ));
                                if ( $post_id && !is_wp_error( $post_id ) ) {
                                    $p_id = $this->set_program_school( $program_item, $post_id);
                                }
                            }
                        } else {
                            $p_id = $this->set_program_school( $program_item, $career_id );
                        }

                    }
                     */


                } // End if inserted school

            }

        } //End foreach $data['values']

        // Delete schools not in the sheet
        if ( ! empty( $schools ) ) {
            $ids_not_in = 'NOT IN('.join( ',', $schools ).')';
            global $wpdb;

            /*
            $sql = "DELETE FROM $this->table
              WHERE school_id IN (
                    SELECT school_id
                    FROM $this->table_relationships
                    WHERE school_id $ids_not_in AND career_id = %d
                )";
            $wpdb->query( $wpdb->prepare( $sql, $career_id ) );
            */

            // Delete from table_relationships
            $sql = "DELETE FROM $this->table_relationships WHERE school_id $ids_not_in AND career_id = %d AND program_slug = %s ";
            $wpdb->query( $wpdb->prepare( $sql, $career_id, $args['program_slug'] ) );
        }

        return $schools_data;
    }

    function get_school_by_name( $school_name ){
        global $wpdb;
        $sql = "SELECT * FROM $this->table
        WHERE school_name LIKE %s";
        $r = $wpdb->get_row( $wpdb->prepare( $sql, $school_name ), ARRAY_A );
        return $r;
    }
    function insert_school( $data = array() ){
        global $wpdb;
        // Do not insert school if it have no name.
        if ( ! isset( $data['school_name'] ) || ! $data['school_name'] ) {
            return false;
        }
        $r = $wpdb->insert(
            $this->table,
            $data
        );
        $id = false;
        if ( $r ){
            $id = $wpdb->insert_id;
        }

        return $id;
    }

    function update_school( $school_id, $data ){
        global $wpdb;
        return $wpdb->update(
            $this->table,
            $data,
            array( 'school_id' => $school_id )
        );
    }

    function get_program_school( $data, $career_id = null ){
        global $wpdb;
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        $row = $wpdb->get_row(
            $wpdb->prepare( "SELECT * FROM {$this->table_relationships}
                WHERE school_id = %d AND career_id = %d AND program_slug = %s AND degree_level = %s",
                $data['school_id'],
                $career_id,
                $data['program_slug'],
                $data['degree_level']
            ),
            ARRAY_A );
        return $row;
    }

    function set_program_school( $data = array(), $career_id = null ){
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        if ( ! $career_id ) {
            return false;
        }
        $data['career_id'] = $career_id;
        if ( ! isset( $data['school_id'] ) ) {
            return false;
        }

        if ( ! isset( $data['degree_level'] ) || ! $data['degree_level'] ) {
            return false;
        }

        global $wpdb;

        $row = $this->get_program_school( $data );
        if ( ! empty( $row ) ) {
            $wpdb->update(
                $this->table_relationships,
                $data,
                array(
                    'program_id' => $row['program_id'],
                )
            );
            return $row['program_id'];

        } else {
            $r = $wpdb->insert(
                $this->table_relationships,
                $data
            );
            if ( $r ){
               return $wpdb->insert_id;
            }
        }

        return false;
    }

}