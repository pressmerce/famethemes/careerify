<?php

class Careerify_Shortcode_USMap {

    function scripts( $career_id =  false,  $data = array() ){
        wp_register_script('career_raphael', CAREERIFY_URL. '/assets/js/raphael.min.js', array( 'jquery' ), '1.0.0', true );
        wp_register_script('career_usmap', CAREERIFY_URL. '/assets/js/jquery.usmap.js', array( 'jquery', 'career_raphael' ), '1.0.0', true );
        wp_register_script('career_frontend', CAREERIFY_URL. '/assets/js/frontend.js', array( 'jquery', 'career_raphael', 'career_usmap' ), '1.0.0', true );

        wp_enqueue_script('underscore');
        wp_enqueue_script('backbone');
        wp_enqueue_script('career_raphael');
        wp_enqueue_script('career_usmap');
        wp_enqueue_script('career_frontend');
        if ( $career_id && ! empty( $data ) ) {
            wp_localize_script( 'career_usmap', 'career_usmap_'.$career_id, $data );
        }
        add_action( 'wp_footer', array( $this, 'tooltip_template' ) );

    }
    function tooltip_template(){
        ?>
        <script id="career-usmap-tooltip-tpl" type="text/html">
            <div class="state-tooltip state-{{ data.state_code }} state-{{ data.state_code }} map-id-{{ data.career_id }}">
                <div class="state-tooltip-content">
                    <div class="tooltip-field">
                        <div class="label-full">{{ data.state_name }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Employment number', 'careerify' ); ?></div>
                        <div class="value">{{ data.employment_number }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Hourly mean wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.hourly_mean_wage }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Annual mean wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.annual_mean_wage }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Annual 10th percentile wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.annual_10th_percentile_wage }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'annual 25th percentile wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.annual_25th_percentile_wage }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Annual 75th percentile wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.annual_75th_percentile_wage }}</div>
                    </div>
                    <div class="tooltip-field">
                        <div class="label"><?php esc_html_e( 'Annual 90th percentile wage', 'careerify' ); ?></div>
                        <div class="value">{{ data.annual_90th_percentile_wage }}</div>
                    </div>
                </div>
            </div>
        </script>
        <?php
    }
    function add_scripts(){
        add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
    }
    function render_content( $career_id = null ){
        return '<div class="us-map" data-id="'.esc_attr( $career_id ).'"></div>';
    }

    /**
     * Get note from sheet value
     *
     * @param $value
     * @param string $type
     * @return float|int
     */
    function get_note( $value, $type = 'year' ) {
        return careerify_get_salary_note( $value, $type );
    }

    static function shortcode( $atts, $content = false ){

        $atts = shortcode_atts( array(
            'career' => '',
            'id' => '',
        ), $atts );

        $career_id = 0;
        if( $atts['career'] ) {
            $post = get_page_by_path( untrailingslashit( $atts['career'] ) , OBJECT, 'career');
            if ( $post ) {
                $career_id = $post->ID;
            }
        }

        if ( ! $career_id && $atts[ 'id' ] ) {
            $career_id = $atts['id'];
        }

        if ( ! $career_id ) {
            global $post;
            $career_id = $post->ID;
        }

        if ( ! $career_id ) {
            return false;
        }

        $self = new self();

        $s = new Careerify_Career_Salary_States( $career_id );
        $data = $s->get_list_salary( array( 'per_page' => -1 ) );

        $array = array();
        $keys = array(
            'annual_mean_wage' => 'year',
            'annual_10th_percentile_wage' => 'year',
            'annual_25th_percentile_wage' => 'year',
            'annual_75th_percentile_wage' => 'year',
            'annual_90th_percentile_wage' => 'year',
        );
        foreach ( $data as $item ) {
            $item['state_name'] = Careerify_Address::get_state_by_code( $item['state_code'] );
            foreach ( $keys as $k => $type ) {
                if ( isset( $item[ $k ] ) ) {
                    $t = $self->get_note( $item[ $k ], $type );
                    if ( $t > -1 ) {
                        $item[ $k ] = $t;
                    }
                }
            }
            $array[ $item['state_code'] ] = apply_filters( 'careerify_usmap_item', $item );
        }

        $self->scripts( $career_id, $array );

        return $self->render_content( $career_id );
    }
}

add_shortcode( 'careerify_usmap', array( 'Careerify_Shortcode_USMap', 'shortcode' ) );