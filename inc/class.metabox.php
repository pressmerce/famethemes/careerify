<?php
/**
 * Register a meta box using a class.
 */
class Carreerify_Meta_Box {

    /**
     * Constructor.
     */
    public function __construct() {
        if ( is_admin() ) {
            add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
            add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );

            add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );
            //add_action( 'admin_footer', array( $this, 'modal' ) );
        }
    }

    function load_scripts( $hook ) {
        wp_register_style('career_admin', CAREERIFY_URL. '/assets/css/admin.css', false, '1.0.0');
        wp_enqueue_style('career_admin');
        if ( 'career' == get_post_type() && ( $hook == 'post-new.php' || 'post.php' == $hook ) ) {
            wp_register_script('career_admin', CAREERIFY_URL. '/assets/js/admin-career.js', array( 'jquery' ), '1.0.0', true );
            wp_enqueue_script('career_admin');
            wp_localize_script( 'career_admin', 'CareerAdmin', array(
                'nonce' => wp_create_nonce( 'career_admin' ),
                'sync' => esc_html__( 'Sync', 'careerify' ),
                'sync_status' => esc_html__( 'Syncing...%s', 'careerify' ),
            ) );

        }
    }

    /**
     * Meta box initialization.
     */
    public function init_metabox() {
        add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
        add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );
    }

    /**
     * Adds the meta box.
     */
    public function add_metabox() {
        add_meta_box(
            'careerify_schools',
            esc_html__( 'Schools', 'careerify' ),
            array( $this, 'render_schools' ),
            'career',
            'side',
            'low'
        );

        add_meta_box(
            'careerify_salary',
            esc_html__( 'Salary for cities', 'careerify' ),
            array( $this, 'render_salary_cities' ),
            'career',
            'side',
            'low'
        );

        add_meta_box(
            'careerify_salary_states',
            esc_html__( 'Salary for states', 'careerify' ),
            array( $this, 'render_salary_states' ),
            'career',
            'side',
            'low'
        );

    }

    static function modal_schools( $post_id = 0 ){
        $id = $post_id ? 'get_schools-id-'.$post_id : '';
        $sheets = get_post_meta( $post_id, '_careerify_school_sheets', true );
        if ( ! is_array( $sheets ) ) {
            $sheets = array();
        }
        if ( ! count( $sheets ) ) {
            $sheets = array(
                array(
                    'name' => '',
                    'url' => ''
                )
            );
        }
        ?>
        <div id="<?php echo esc_attr( $id ); ?>" class="career-modal-wrapper" tabindex="0">
            <div class="media-modal career-modal wp-core-ui">
                <button type="button" class="button-link media-modal-close">
                    <span class="media-modal-icon"><span class="screen-reader-text"><?php esc_html_e( 'Close media panel', 'careerify' ); ?></span></span>
                </button>
                <div class="media-modal-content">
                    <div class="media-frame mode-select wp-core-ui hide-menu hide-toolbar hide-router">

                        <div class="media-frame-title">
                            <h1><?php esc_html_e( 'Schools', 'careerify' ); ?><span class="dashicons dashicons-arrow-down"></span></h1>
                        </div>

                        <div class="media-frame-content">
                            <div class="media-frame-content-inner">

                                <div class="unlimited-inputs">
                                    <?php foreach ( $sheets as $k => $sheet ) { ?>
                                    <div class="sheet-input career-schools-sheet-input">
                                        <input class="text program-name" type="text" value="<?php echo esc_attr( $sheet['name'] ) ; ?>" placeholder="<?php esc_html_e( 'Program name', 'careerify' ); ?>">
                                        <input class="text program-sheet" data-post-id="<?php echo esc_attr( $post_id ); ?>" value="<?php echo esc_attr( $sheet['url'] ) ; ?>" placeholder="<?php esc_html_e( 'Google sheet url', 'careerify' ); ?>" type="text">
                                        <a href="#" class="remove-item button button-secondary" data-post-id="<?php echo esc_attr( $post_id ); ?>"><?php esc_html_e( '-', 'careerify' ); ?></a>
                                        <a href="#" class="sync button button-secondary" data-post-id="<?php echo esc_attr( $post_id ); ?>"  ><?php esc_html_e( 'Sync', 'careerify' ); ?></a>
                                    </div>
                                    <?php } ?>
                                    <a href="#" class="new-item"><?php esc_html_e( 'Add new', 'careerify' ); ?></a>
                                </div>

                                <div class="ajax-table-data">
                                <?php
                                $list_table = new Career_Schools_Table( $post_id );
                                //Fetch, prepare, sort, and filter our data...
                                $list_table->prepare_items();

                                $list_table->display();
                                ?>
                                </div>
                            </div><!-- /.media-frame-content-inner -->

                        </div>

                    </div>
                </div>
            </div>
            <div class="media-modal-backdrop"></div>
        </div>
        <?php
    }

    static function modal_salary_cities( $post_id = 0 ){
        $id = $post_id ? 'get_salary_cities-id-'.$post_id : '';
        $google_sheet_url = get_post_meta( $post_id, '_careerify_salary_cities_google_sheet_url', true );
        ?>
        <div id="<?php echo esc_attr( $id ); ?>" class="career-modal-wrapper" tabindex="0">
            <div class="media-modal career-modal wp-core-ui">
                <button type="button" class="button-link media-modal-close">
                    <span class="media-modal-icon"><span class="screen-reader-text"><?php esc_html_e( 'Close media panel', 'careerify' ); ?></span></span>
                </button>
                <div class="media-modal-content">
                    <div class="media-frame mode-select wp-core-ui hide-menu hide-toolbar hide-router">

                        <div class="media-frame-title">
                            <h1><?php esc_html_e( 'Salary for cities', 'careerify' ); ?><span class="dashicons dashicons-arrow-down"></span></h1>
                        </div>

                        <div class="media-frame-content">
                            <div class="media-frame-content-inner">

                                <div class="sheet-input career-salary-sheet-input">
                                    <input class="text" data-post-id="<?php echo esc_attr( $post_id ); ?>" value="<?php echo esc_attr( $google_sheet_url ); ?>" placeholder="<?php esc_html_e( 'Google sheet url', 'careerify' ); ?>" type="text">
                                    <a href="#" data-action="sync_salary_cities" class="button button-secondary"><?php esc_html_e( 'Sync', 'careerify' ); ?></a>
                                </div>
                                <div class="ajax-table-data">
                                    <?php

                                    $list_table = new Career_Salary_Cites_Table( $post_id );
                                    //Fetch, prepare, sort, and filter our data...
                                    $list_table->prepare_items();

                                    $list_table->display();
                                    ?>
                                </div>
                            </div><!-- /.media-frame-content-inner -->
                        </div>

                    </div>
                </div>
            </div>
            <div class="media-modal-backdrop"></div>
        </div>
        <?php
    }

    static function modal_salary_states( $post_id = 0 ){
        $id = $post_id ? 'get_salary_states-id-'.$post_id : '';
        $google_sheet_url = get_post_meta( $post_id, '_careerify_salary_states_google_sheet_url', true );
        ?>
        <div id="<?php echo esc_attr( $id ); ?>" class="career-modal-wrapper" tabindex="0">
            <div class="media-modal career-modal wp-core-ui">
                <button type="button" class="button-link media-modal-close">
                    <span class="media-modal-icon"><span class="screen-reader-text"><?php esc_html_e( 'Close media panel', 'careerify' ); ?></span></span>
                </button>
                <div class="media-modal-content">
                    <div class="media-frame mode-select wp-core-ui hide-menu hide-toolbar hide-router">

                        <div class="media-frame-title">
                            <h1><?php esc_html_e( 'Salary for states', 'careerify' ); ?><span class="dashicons dashicons-arrow-down"></span></h1>
                        </div>

                        <div class="media-frame-content">
                            <div class="media-frame-content-inner">

                                <div class="sheet-input career-salary-sheet-input">
                                    <input class="text" data-post-id="<?php echo esc_attr( $post_id ); ?>" value="<?php echo esc_attr( $google_sheet_url ); ?>" placeholder="<?php esc_html_e( 'Google sheet url', 'careerify' ); ?>" type="text">
                                    <a href="#" data-action="sync_salary_states" class="button button-secondary"><?php esc_html_e( 'Sync', 'careerify' ); ?></a>
                                </div>
                                <div class="ajax-table-data">
                                    <?php

                                    $list_table = new Career_Salary_States_Table( $post_id );
                                    //Fetch, prepare, sort, and filter our data...
                                    $list_table->prepare_items();

                                    $list_table->display();
                                    ?>
                                </div>
                            </div><!-- /.media-frame-content-inner -->
                        </div>

                    </div>
                </div>
            </div>
            <div class="media-modal-backdrop"></div>
        </div>
        <?php
    }

    /**
     * Renders the meta box.
     */
    public function render_salary_cities( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'custom_nonce_action', 'careerify_nonce' );
        if ( 'auto-draft' ==  $post->post_status ) {
            esc_html_e( 'Save your career to import salary', 'careerify' );
        } else {
            ?>
            <a class="careerify_career_modal careerify_career_salary" data-action="get_salary_cities" data-id="<?php echo esc_attr( $post->ID ); ?>" href="#"><?php esc_html_e( 'View Salary', 'careerify' ); ?></a>
            <?php
        }

    }

    /**
     * Renders the meta box.
     */
    public function render_salary_states( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'custom_nonce_action', 'careerify_nonce' );
        if ( 'auto-draft' ==  $post->post_status ) {
            esc_html_e( 'Save your career to import salary', 'careerify' );
        } else {
            ?>
            <a class="careerify_career_modal careerify_career_salary" data-action="get_salary_states" data-id="<?php echo esc_attr( $post->ID ); ?>" href="#"><?php esc_html_e( 'View Salary', 'careerify' ); ?></a>
            <?php
        }

    }

    /**
     * Renders the meta box.
     */
    public function render_schools( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'careerify_nonce_action', 'careerify_nonce' );
        if ( 'auto-draft' ==  $post->post_status ) {
            esc_html_e( 'Save your career to import schools', 'careerify' );
        } else {

            $cs = new Careerify_Career_Schools( $post->ID );
            $n = $cs->count_schools( );
            ?>
            <a data-id="<?php echo esc_attr( $post->ID ); ?>" data-action="get_schools" class="careerify_career_modal careerify_career_schools" href="#"><?php printf( _n( '%d School', '%d Schools', $n, 'careerify' ), $n ); ?></a>
            <?php

        }

    }

    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post ) {
        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST['careerify_nonce'] ) ? $_POST['careerify_nonce'] : '';
        $nonce_action = 'careerify_nonce_action';

        // Check if nonce is set.
        if ( ! isset( $nonce_name ) ) {
            return;
        }

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }

        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }

        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }
    }
}

new Carreerify_Meta_Box();