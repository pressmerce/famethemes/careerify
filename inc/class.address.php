<?php

class Careerify_Address {

    static $states = array (
            'AL'=>'Alabama',
            'AK'=>'Alaska',
            'AZ'=>'Arizona',
            'AR'=>'Arkansas',
            'CA'=>'California',
            'CO'=>'Colorado',
            'CT'=>'Connecticut',
            'DE'=>'Delaware',
            'DC'=>'District Of Columbia',
            'FL'=>'Florida',
            'GA'=>'Georgia',
            'HI'=>'Hawaii',
            'ID'=>'Idaho',
            'IL'=>'Illinois',
            'IN'=>'Indiana',
            'IA'=>'Iowa',
            'KS'=>'Kansas',
            'KY'=>'Kentucky',
            'LA'=>'Louisiana',
            'ME'=>'Maine',
            'MD'=>'Maryland',
            'MA'=>'Massachusetts',
            'MI'=>'Michigan',
            'MN'=>'Minnesota',
            'MS'=>'Mississippi',
            'MO'=>'Missouri',
            'MT'=>'Montana',
            'NE'=>'Nebraska',
            'NV'=>'Nevada',
            'NH'=>'New Hampshire',
            'NJ'=>'New Jersey',
            'NM'=>'New Mexico',
            'NY'=>'New York',
            'NC'=>'North Carolina',
            'ND'=>'North Dakota',
            'OH'=>'Ohio',
            'OK'=>'Oklahoma',
            'OR'=>'Oregon',
            'PA'=>'Pennsylvania',
            'RI'=>'Rhode Island',
            'SC'=>'South Carolina',
            'SD'=>'South Dakota',
            'TN'=>'Tennessee',
            'TX'=>'Texas',
            'UT'=>'Utah',
            'VT'=>'Vermont',
            'VA'=>'Virginia',
            'WA'=>'Washington',
            'WV'=>'West Virginia',
            'WI'=>'Wisconsin',
            'WY'=>'Wyoming',
            // In addition to the states, several other areas belong to the United States: https://simple.wikipedia.org/wiki/List_of_U.S._states
            'AS' => 'American Samoa',
            'GU' => 'Guam',
            'MP' => 'Northern Mariana Islands',
            'PR' => 'Puerto Rico',
            'VI' => 'Virgin Islands',
        );

    static function get_states(){
        return self::$states;
    }

    static function get_state_by_code( $code ){
        if ( isset( self::$states[ $code ] ) ) {
            return self::$states[ $code ];
        } else {
            return false;
        }
    }

    static function get_state_code( $name ){
        $sates = array_flip( array_map( 'strtolower', self::$states ) );
        $name = strtolower( $name );
        if ( isset( $sates[ $name ] ) ) {
            return  $sates[ $name ];
        } else {
            return false;
        }
    }

    static function parse_area_name( $area_name ) {
        $return = false;
        if ( strpos( $area_name, 'area' ) > 0 ) { // area example: Northwest Alabama nonmetropolitan area
            while( $state_name = current( self::$states ) ) {
                $code =  key( self::$states  );
                if ( strpos( strtolower( $area_name ),  strtolower( $state_name ) ) !== false ) {
                    $return = array(
                        'state_code' => $code,
                        'state_name' => self::get_state_by_code( $code ),
                        'cities' => array( $area_name ),
                    );
                    return $return;
                }
                next( self::$states );
            }
        } elseif ( strpos( $area_name, 'Metropolitan Division' ) > 0 ) { // area example: Silver Spring-Frederick-Rockville MD Metropolitan Division
            $area_name = trim( str_replace( 'Metropolitan Division', '', $area_name ) );
            $arr = explode( ' ', $area_name );
            $n = count( $arr );
            $tpl_states = $arr[ $n - 1 ];
            unset( $arr[  $n - 1 ] );
            $arr = join( ' ', $arr );
            $arr = trim( $arr );
            $cities = explode( '-', $arr );
            $cities = array_filter( $cities );

            $tpl_states = explode( '-', $tpl_states );
            $code = current( $tpl_states );
            $code = strtoupper( $code  );
            if ( isset( self::$states[ $code ]  ) ) {
                $return = array(
                    'state_code' => $code,
                    'state_name' => self::$states[ $code ],
                    'cities' => $cities,
                );
                return $return;
            }
        } else { // Area like this: San Luis Obispo-Paso Robles-Arroyo Grande CA
            $arr = explode( ' ', $area_name );
            $n = count( $arr );
            $tpl_states = $arr[ $n - 1 ];
            unset( $arr[ $n - 1 ] );
            $arr = join( ' ', $arr );
            $arr = trim( $arr );
            $arr = str_replace( ',', '', $arr );
            $tpl_states = explode( '-', $tpl_states );
            $cities = explode( '-', $arr );
            $cities = array_filter( $cities );
            $code = current( $tpl_states );
            $code = strtoupper( $code  );
            if ( isset( self::$states[ $code ]  ) ) {
                $return = array(
                    'state_code' => $code,
                    'state_name' => self::$states[ $code ],
                    'cities' => $cities,
                    );
            }
            return $return;
        }

       return $return;
    }


}