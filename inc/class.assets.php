<?php

class Careerify_Assets {
    function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
    }
    /**
     * Enqueue script with script.aculo.us as a dependency.
     */
    function scripts() {
        wp_register_style( 'careerify', CAREERIFY_URL. '/assets/css/frontend.css', false, '1.0.0');
        wp_enqueue_style( 'careerify' );
    }

}

new Careerify_Assets();