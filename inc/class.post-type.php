<?php

class Careerify_Post_Type {

    function __construct() {
        add_action( 'init',array( $this, 'register' ) );
        add_action( 'before_delete_post',array( $this, 'before_delete_post' ) );
    }

    function before_delete_post( $post_id ){
        if ( get_post_type( $post_id ) != 'career' ) {
            return ;
        }

        $sc = new Careerify_Career_Schools( $post_id );
        $sc->delete_career_schools();

        $csc = new Careerify_Career_Salary_Cites( $post_id );
        $csc->delete_career_salary( $post_id );

        $css = new Careerify_Career_Salary_States( $post_id );
        $css->delete_career_salary( $post_id );


    }

    function register(){
        $labels = array(
            'name'               => _x( 'Careers', 'post type general name', 'careerify' ),
            'singular_name'      => _x( 'Career', 'post type singular name', 'careerify' ),
            'menu_name'          => _x( 'Careers', 'admin menu', 'careerify' ),
            'name_admin_bar'     => _x( 'Career', 'add new on admin bar', 'careerify' ),
            'add_new'            => _x( 'Add New', 'career', 'careerify' ),
            'add_new_item'       => __( 'Add New Career', 'careerify' ),
            'new_item'           => __( 'New Career', 'careerify' ),
            'edit_item'          => __( 'Edit Career', 'careerify' ),
            'view_item'          => __( 'View Career', 'careerify' ),
            'all_items'          => __( 'All Careers', 'careerify' ),
            'search_items'       => __( 'Search Careers', 'careerify' ),
            'parent_item_colon'  => __( 'Parent Careers:', 'careerify' ),
            'not_found'          => __( 'No Careers found.', 'careerify' ),
            'not_found_in_trash' => __( 'No Careers found in Trash.', 'careerify' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => esc_html__( 'Description.', 'careerify' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'career' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' )
        );
        register_post_type( 'career', $args );
    }
}

$GLOBALS['Careerify_Post_Type'] = new Careerify_Post_Type();