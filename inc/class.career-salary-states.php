<?php

class Careerify_Career_Salary_States {

    public $career_id;
    public $table;

    private $salary_fields = array(
        'csd_id',
        'career_id',
        'state_code',
        'employment_number',
        'hourly_mean_wage',
        'annual_mean_wage',
        'annual_10th_percentile_wage',
        'annual_25th_percentile_wage',
        'annual_75th_percentile_wage',
        'annual_90th_percentile_wage',
    );

    function __construct( $career_id = 0 ) {
        $this->set_career_id( $career_id );
        global $table_prefix;
        $this->table = $table_prefix.'careerify_salary_states';
    }
    function set_career_id( $career_id ) {
        $this->career_id = absint( $career_id );
    }


    function count_career_salary( $career_id = 0){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }
        global $wpdb;
        $sql = "SELECT count( csd_id ) as found_items FROM $this->table
        WHERE career_id = %d";
        $r = $wpdb->get_var( $wpdb->prepare( $sql, $career_id ) );
        return $r;
    }

    function get_list_salary( $get_args = array() , $career_id = 0, $output = 'ARRAY_A' ){
        global $wpdb;
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        $get_args = wp_parse_args( $get_args,array(
            'per_page'  => 100,
            'paged'     => 1,
            'orderby'   => 'state_code',
            'order'     => 'ASC'
        ) );
        extract( $get_args );

        if ( ! $order )  {
            $order = 'ASC';
        }

        if ( strtolower( $order ) != 'asc' ) {
            $order = 'desc';
        }
        if ( ! $orderby ) {
            $orderby = 'state_code';
        }

        if ( ! in_array( $orderby, $this->salary_fields ) ) {
            $orderby = 'state_code';
        }

        if ( $paged <= 0 ) {
            $paged = 1;
        }

        $sql = "SELECT * FROM $this->table AS cs
        WHERE cs.career_id = %d
        ORDER BY $orderby $order";

        if ( $per_page > 0 ) {
            $offset = ($paged - 1) * $per_page;
            $sql .=" LIMIT %d, %d ";
            $sql = $wpdb->prepare( $sql, $career_id, $offset, $per_page );
        } else {
            $sql = $wpdb->prepare( $sql, $career_id );
        }

        $r = $wpdb->get_results( $sql , $output );

        return $r;
    }

    function delete_career_salary( $career_id = null ){
        if ( ! $career_id ) {
            $career_id = $this->career_id;
        }

        global $wpdb;
        $sql = "DELETE FROM $this->table WHERE career_id = %d ";
        $wpdb->query( $wpdb->prepare( $sql, $career_id ) );

    }


    function sync( $google_sheet_url, $force_career_id = null, $update_if_exists = true ) {
        $gs = new Careerify_Google_Sheets();
        $data = $gs->get_sheet_data( $google_sheet_url, 'A2:R' );

        if ( is_wp_error( $data ) ) {
            return false;
        }

        if ( ! is_array( $data ) ) {
            return false;
        }

        if ( ! isset( $data['values'] ) ) {
            $data['values'] = array();
        }

        if ( true === $force_career_id ) {
            $career_id = $this->career_id;
        } else {
            $career_id = $force_career_id;
        }

        $ids = array();

        $count = 0; // Count number schools imported
        foreach ( ( array ) $data['values'] as $k => $item ) {
            if ( count( $item ) < 15 ) {
                continue;
            }

            $area_name = $item[ 0 ];
            echo $area_name;
            $state_code = Careerify_Address::get_state_code( $area_name );

            $tpl_item = array(
                'csd_id' => '',
                'career_id' => $career_id,
                'state_code' => $state_code,
                'employment_number' => $item[1],
                'hourly_mean_wage' => $item[3],
                'annual_mean_wage' => $item[4],
                'annual_10th_percentile_wage' => $item[11],
                'annual_25th_percentile_wage' => $item[12],
                'annual_75th_percentile_wage' => $item[14],
                'annual_90th_percentile_wage' => $item[15],
            );

            $id = $this->set_salary_for_state( $tpl_item, $career_id );
            if ( $id ) {
                $count ++ ;
                $ids[] = $id;
            }

        } //End foreach $data['values']

        // Delete schools not in the sheet
        if ( ! empty( $ids ) ) {
            $ids_not_in = 'NOT IN('.join( ',', $ids ).')';
            global $wpdb;

            $sql = "DELETE FROM $this->table WHERE csd_id $ids_not_in AND career_id = %d ";
            $wpdb->query( $wpdb->prepare( $sql, $career_id ) );

        }
        return $count;

    }

    function get_salary_by_state( $state_code , $career_id = null ) {
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        global $wpdb;
        $sql =  "SELECT * FROM {$this->table} as t
          WHERE t.state_code LIKE %s AND t.career_id = %d";
        $row = $wpdb->get_row( $wpdb->prepare( $sql , $state_code, $career_id ) , ARRAY_A );
        return $row;
    }

    function set_salary_for_state( $salary_data = array(), $career_id = null ){
        if ( ! $career_id ) {
            $career_id =  $this->career_id;
        }
        if ( ! $career_id ) {
            return false;
        }

        if ( ! $salary_data['state_code'] ) {
            return false;
        }

        $check = $this->get_salary_by_state( $salary_data['state_code'], $career_id );
        global $wpdb;

        unset( $salary_data['csd_id'] );

        if ( $check ) {
            $id = $check['csd_id'];
            $wpdb->update( $this->table, $salary_data, array(
                    'csd_id'  => $id,
                ) );
            return $id;
        }


        $r = $wpdb->insert ( $this->table, $salary_data );
        if ( $r ) {
            $id = $wpdb->insert_id;
        } else {
            $id = false;
        }
        return $id;

    }

}