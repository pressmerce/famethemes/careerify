<?php

class Careerify_Google_Sheets {

    private function get_sheet_id( $url ){
        $m = array();
        //$url = 'https://docs.google.com/spreadsheets/d/1N9lBj5EOFP1rrZsYWvwvwv4dfMELey7ydPTGEEzUUh8/edit#gid=1368265306';
        preg_match('!https://(?:docs\.google\.com/spreadsheets/d/|script\.google\.com/macros/s/)([^/]+)!', $url, $m);
        if (!empty($m[1])) {
            $id = $m[1];
        } else {
            $id = sanitize_title_with_dashes( $url );
        }
        return $id;
    }

    function get_sheet_data( $sheer_url, $range = 'A:R' ){
        $api_key = 'AIzaSyCc5_1vmw7gN4T7w8L6Xo04-ZroO1tz5N4';
        $range = urlencode( $range );
        $sheet_id = $this->get_sheet_id( $sheer_url );
        $remote_url = 'https://sheets.googleapis.com/v4/spreadsheets/'.$sheet_id.'/values/'.$range.'?key='.$api_key;

        $response = wp_remote_get( $remote_url );
        $code = wp_remote_retrieve_response_code( $response );
        if ( $code != 200 ) {
            $result = new WP_Error( 'sheets_not_found', esc_html__( 'Google sheet not found', 'careerify' ) );
        } else {
            $result = wp_remote_retrieve_body( $response );
            if ( is_string( $result ) ) {
                $result = json_decode( $result, true );
            }
        }

        return $result;

    }

}