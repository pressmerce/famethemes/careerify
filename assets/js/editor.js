//alert( 'ok' );

jQuery( document ).ready( function( $ ) {
    var $body = $( 'body' );


    $body.on( 'click', '.career-modal-shortcode .media-modal-close', function(e ){
        e.preventDefault();
        $( '.career-modal-shortcode, .shortcode-drop').addClass( 'hidden' );
    } );

    $body.on( 'click', '.insert-media-career', function(e ){
        e.preventDefault();
        $( '.career-modal-shortcode, .shortcode-drop').removeClass( 'hidden' );
    } );


    $body.on( 'click', '.insert-career-shortcode', function(e ){
        e.preventDefault();
        var s = $( this ).parent().find( '.shortcode-career-generated').val();
        if ( window.send_to_editor ) {
            window.send_to_editor ( s );
            // close modal
            $( '.career-modal-shortcode, .shortcode-drop').addClass( 'hidden' );
        }
    } );


    $body.on( 'click', '.shortcode-creator select', function(){
        var s =  $( this).closest( '.shortcode-creator' );
        var shortcode = s.find( 'select.shortcode-name').val();
        var state = s.find( 'select.shortcode-state').val();
        var career = s.find( 'select.shortcode-career').val();

        if ( shortcode == 'careerify_usmap' ) {
            s.find( 'select.shortcode-state').hide();
            s.find( 'select.shortcode-state option').removeAttr( 'selected' );
            state = '';
        } else {
            s.find( 'select.shortcode-state').show();
        }

        var text = [];
        if ( shortcode && career ) {
            text[0] = shortcode;
            if ( career ) {
                text[1] = 'career="'+career+'"';
            }

            if ( state ) {
                text[2] = 'state="'+state+'"';
            }

            text = '['+text.join(' ')+']';
        } else {
            text = '';
        }

        $( '.shortcode-career-generated').val( text );
    } );




} );