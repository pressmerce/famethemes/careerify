jQuery(document).ready(function($) {

    /**
     * Function that loads the Mustache template
     */
    var _get_template = _.memoize( function ( template_id, data ) {
        var compiled,
        /*
         * Underscore's default ERB-style templates are incompatible with PHP
         * when asp_tags is enabled, so WordPress uses Mustache-inspired templating syntax.
         *
         * @see trac ticket #22344.
         */
        options = {
            evaluate: /<#([\s\S]+?)#>/g,
            interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
            escape: /\{\{([^\}]+?)\}\}(?!\})/g,
            variable: 'data'
        };

        return function ( template_id,  data ) {
            var tpl = $( template_id ).html();
            compiled = _.template( tpl , null, options );
            return compiled( data );
        };
    });
    var get_template = _get_template();

    window.mx = 0;
    window.my = 0;
    $(document).mousemove(function(e) {
        window.mx = e.clientX;
        window.my = e.clientY;
    }).mouseover(); // call the handler immediately

    $( 'body').on( 'click', '.state-tooltip', function(){
        $( '.state-tooltip').hide();
    } );

    $( '.us-map').each( function(){
        var career_id = $( this).attr( 'data-id' );

        var usmapHover =  function( event, data, eventType ) {
            // Output the abbreviation of the state name to the console
            //console.log(data.name);
            if ( typeof eventType === "undefined" || ! eventType ) {
                eventType = 'hover';
            }
            var state_code = data.name;
            var tooltip;

            if ( $( '.state-tooltip.state-'+state_code+'.map-id-'+career_id ).length > 0 ) {
                tooltip = $( '.state-tooltip.state-'+state_code+'.map-id-'+career_id );
            } else {
                if ( window[ 'career_usmap_' + career_id ] ) {
                    if ( window[ 'career_usmap_' + career_id ][ state_code ] ) {
                        var tooltip = get_template( '#career-usmap-tooltip-tpl', window[ 'career_usmap_' + career_id ][ state_code ] );
                        tooltip = $( tooltip );
                        $( 'body').append( tooltip );
                    }
                }
            }

            if ( tooltip ) {
                var h = tooltip.height();
                var w = tooltip.width();
                var top = window.my - ( h / 2 ) - 5;
                var left = window.mx + 10;
                if ( top < 30 ) {
                    top = 30;
                }
                tooltip.css( {
                    top: top,
                    left: left,
                    display: 'block',
                } );
                var ww = $( window).width();
                if ( eventType == 'click' ) {
                    left =  ww/ 2;
                    tooltip.css( {
                        left: left,
                    } );
                } else {

                    var of = tooltip.offset();
                    if ( of.left + w > ww ) {
                        left =  of.left - ( w + 15 );
                    }
                    if ( left < 0 ) {
                        left = 0;
                    }
                    tooltip.css( {
                        left: left,
                    } );
                }

            }
        };

        $( this ).usmap({
            useAllLabels: true,
            //showLabels: true,
            stateStyles: {
                fill: '#24F7D3',
                stroke: '#333333',
                scale: [1, 1]
            },
            stateHoverStyles: {
                fill: "#f7906f",
                stroke: "#333333",
                scale: [2.5, 2.5]
            },
            labelBackingStyles: {
                fill: "#24F7D3",
                stroke: "#333333",
                "stroke-width": 1,
                "stroke-linejoin": "round",
                scale: [1, 1]
            },
            // The styles for the hover
            labelBackingHoverStyles: {
                fill: "#C7F464",
                stroke: "#ADCC56",
                'font-size': '10px',
            },
            labelTextStyles: {
                fill: "#333333",
                'stroke': 'none',
                'font-weight': 300,
                'stroke-width': 0,
                'font-size': '6px',
                'color': '#fff'
            },
            click: function( event, data ){
                $( '.state-tooltip.map-id-'+career_id).hide();
                usmapHover( event, data );
            },
            mouseover: usmapHover,
            mouseout: function(event, data) {
                // Output the abbreviation of the state name to the console
                // console.log(data.name);
                $( '.state-tooltip.map-id-'+career_id).hide();
            }
        });
    } );

    $( window).resize( function(){
        $( '.us-map').each( function(){
            $( this).find( 'svg').attr( 'width', 'auto' ).attr( 'width', '100%' );
        });
    } );

    $( window ).trigger( 'resize' );



    //-----------------------------------------------
    function update_compare_chart( area, setupData ){
        var tpl = get_template( '#career-compare-chart-tpl', setupData );
        var chart_area = $( tpl );
        area.html( chart_area );
    }


    $( 'body').on( 'change', '.compare-chart select[name="state"]', function(){
        var state_code =  $( this).val();
        var career_id = $( this).attr( 'data-id' ) || false;
        var area = $( this).closest( '.compare-chart' );

        $( 'select.compare-city', area ).html( '<option>'+career_chart_settings.loading+'</option>' );
        if (  ! state_code ) {
            var setupData = {
                career_id: career_id,
                states: us_states,
                number_city: 3, // number of city to compare
                cities: {},
                selected_state: state_code,
            };
            update_compare_chart( area, setupData );
            return ;
        }

        var state_data;
        if ( window['states_data'] ) {
            if ( window['states_data'][ state_code ] ) {
                state_data = window['states_data'][ state_code ];
            }
        }

        if ( state_data ) {
            var setupData = {
                career_id: career_id,
                states: us_states,
                number_city: 3, // number of city to compare
                cities: state_data,
                selected_state: state_code,
            };
            update_compare_chart( area, setupData );
        } else {
            $.ajax({
                url: careerify.ajax_url,
                data: {
                    state: state_code,
                    action: 'careerify_frontend_ajax',
                    doing: 'get_salary_state',
                    career_id: career_id,
                },
                dataType: 'json',
                success: function ( res ) {

                    if ( ! res.success ) {
                        return false;
                    }

                    if ( ! window['states_data'] ) {
                        window['states_data'] = {};
                    }

                    window['states_data'][ state_code ] = res.data;

                    var setupData = {
                        career_id: career_id,
                        states: us_states,
                        number_city: 3, // number of city to compare
                        cities: res.data,
                        selected_state: state_code,
                    };

                    update_compare_chart( area, setupData );
                }
            });
        }

    } );


    $( 'body').on( 'change', '.compare-chart select.compare-city', function(){
        var area = $( this).closest( '.compare-chart' );
        var state_code = $( 'select.state-code', area).val();
        if ( ! state_code ) {
            return ;
        }
        var career_id = area.attr( 'data-id' ) || false;
        var cities = [];
        var labels = [];

        var state_data;
        if ( window['states_data'] ) {
            if ( window['states_data'][ state_code ] ) {
                state_data = window['states_data'][ state_code ];
            }
        }

        if ( ! state_data ) {
            return;
        }

        var _count = 0;
        $( 'select.compare-city', area).each( function(){
            var index = $( this).attr( 'data-index' );
            var city_id =  $( this).val();
            if ( state_data[ city_id ] ) {
                cities.push( state_data[ city_id ] );
                labels.push( state_data[ city_id ].area_city  );
                _count ++ ;
            }
        } );

        var city_datasets = {};

        $.each( career_chart_settings.chart_keys, function( key, value ){
            city_datasets[ key ] = [];
            $.each( cities, function( index, city ){
                if ( city[ key ] ) {
                    city_datasets[ key ].push( city[ key ] );
                }
            } );
        } );

        if ( _count ) {
            $( '.compare-chart-content', area).removeClass( 'no-cities' );
            var colors = ['#F08080', '#4682B4', '#32CD32', '#40E0D0', '#FFFF00', '#AFEEEE', 'chartreuse', 'lightblue'];
            var chart_datasets = [];
            var _j = 0;
            $.each(city_datasets, function (key, data) {
                var _data = {
                    type: 'bar',
                    label: career_chart_settings.chart_keys[key],
                    data: data,
                    backgroundColor: colors[_j],
                    borderColor: colors[_j],
                    borderWidth: 1
                };
                _j++;
                chart_datasets.push(_data);
            });

            // Char test-chart
            var ctx = $('.compare-chart', area);
            var chart;

            if (window['chart_' + state_code + career_id]) {
                window['chart_' + state_code + career_id].destroy();
            }

            window['chart_' + state_code + career_id] = new Chart(ctx, {
                type: 'bar',
                // type: 'horizontalBar', // https://jsfiddle.net/2facyuLp/
                data: {
                    labels: labels,
                    datasets: chart_datasets
                },
                options: {
                    responsive: true,

                    legend: {
                        position: 'bottom',
                    },
                    scales: {
                        xAxes: [{
                            // stacked: true
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value, index, values ) {
                                    if (parseInt(value) > 1000) {
                                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return '$' + value;
                                    }
                                }
                            }
                        }]
                    }
                }
            });

            // Insert cities info
            // console.log( cities );
            var _tpl = get_template( '#compare-chart-info-tpl', { cities: cities } );
            _tpl = $( _tpl );
            $( '.compare-chart-info', area).remove();
            $( '.compare-chart-content', area).append( _tpl );

        } else { //End if have cities
            $( '.compare-chart-content', area ).addClass( 'no-cities' );
            $( '.compare-chart-info', area).remove();
        } // End check cities length.

    });


    $( '.compare-chart').each( function(){
        var id = $( this).attr( 'data-id' );
        var setupData = {
            career_id: id,
            states: us_states,
            number_city: 3, // number of city to compare
            cities: {},
        };
        var tpl = get_template( '#career-compare-chart-tpl', setupData );
        var chart_area = $( tpl );
        $( this ).html( chart_area );
    } );

   //---------------------------------------------------------------------

    function update_compare_state_chart( area, setupData ){
        var tpl = get_template( '#career-compare-state-chart-tpl', setupData );
        var chart_area = $( tpl );
        area.html( chart_area );


        // Char test-chart
        var ctx = $('.compare-chart', area);
        var chart;
        var state_code = setupData.selected_state;
        var career_id = setupData.career_id;

        if (window['chart_' + state_code + career_id]) {
            window['chart_' + state_code + career_id].destroy();
        }

        var labels = [], chart_datasets = [];

        $.each( setupData.cities, function( index, city ){
            labels.push( city.area_city );
            chart_datasets.push( city.annual_mean_wage );
        } );
        var height = 30 * chart_datasets.length;
        $( '.compare-chart', area ).attr( 'height' ,  height +'px').css( 'height', height  );

        window['chart_' + state_code + career_id] = new Chart(ctx, {
            type: 'horizontalBar', // https://jsfiddle.net/2facyuLp/
            data: {
                labels: labels,
                datasets: [
                    {
                        label: false,
                        backgroundColor: "rgba(255,99,132,0.2)",
                        borderColor: "rgba(255,99,132,1)",
                        borderWidth: 1,
                        hoverBackgroundColor: "rgba(255,99,132,0.4)",
                        hoverBorderColor: "rgba(255,99,132,1)",
                        data: chart_datasets,
                    }
                ]
            },
            options: {
                responsive: true,
                legend: false,
               // maintainAspectRatio: false,
                barValueSpacing: 20,

                scales: {
                    xAxes: [{
                        barThickness: 30,
                        ticks: {
                            beginAtZero: true,
                            callback: function (value, index, values ) {
                                if (parseInt(value) > 1000) {
                                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                } else {
                                    return '$' + value;
                                }
                            }
                        }
                    }],
                    yAxes: [{
                        //barThickness: 30,
                        padding: 30,
                        //display: false,
                        categorySpacing:.5,
                        barPercentage:.5,
                        //ticks: {
                           // callback: function (value) {
                              //  return value;
                            //}
                        //}
                    }]
                }
            }
        });

    }


    $( 'body').on( 'change', '.compare-chart-state select.state-code', function() {
        var state_code =  $( this).val();
        var career_id = $( this).attr( 'data-id' ) || false;
        var area = $( this).closest( '.compare-chart-state' );
        var setupData;
        $( 'select.compare-city', area ).html( '<option>'+career_chart_settings.loading+'</option>' );
        if (  ! state_code ) {
            setupData = {
                career_id: career_id,
                states: us_states,
                selected_state: state_code,
            };
            update_compare_state_chart( area, setupData );
            area.trigger( 'chart_updated' );
            return ;
        }

        var state_data;
        if ( window['states_data'] ) {
            if ( window['states_data'][ state_code ] ) {
                state_data = window['states_data'][ state_code ];
            }
        }

        if ( state_data ) {
            var setupData = {
                career_id: career_id,
                states: us_states,
                cities: state_data,
                selected_state: state_code,
            };
            update_compare_state_chart( area, setupData );
        } else {
            $.ajax({
                url: careerify.ajax_url,
                data: {
                    state: state_code,
                    action: 'careerify_frontend_ajax',
                    doing: 'get_salary_state',
                    career_id: career_id,
                },
                dataType: 'json',
                success: function ( res ) {

                    if ( ! res.success ) {
                        return false;
                    }

                    if ( ! window['states_data'] ) {
                        window['states_data'] = {};
                    }

                    window['states_data'][ state_code ] = res.data;

                    setupData = {
                        career_id: career_id,
                        states: us_states,
                        cities: res.data,
                        selected_state: state_code,
                    };

                    update_compare_state_chart( area, setupData );
                }
            });
        }

    } ) ;



    $( '.compare-chart-state').each( function(){
        var id = $( this).attr( 'data-id' );
        var setupData = {
            career_id: id,
            states: us_states,
        };
        var default_state = $( this).attr( 'default-state' ) || '';
        var tpl = get_template( '#career-compare-state-chart-tpl', setupData );
        var chart_area = $( tpl );
        $( this ).html( chart_area );

        if ( default_state ) {
            $('select.state-code option', chart_area).removeAttr('selected');
            $('select.state-code option[value="'+default_state+'"]', chart_area).attr('selected', 'selected');
            $('select.state-code', chart_area ).trigger('change');
        }
    } );









    // Char test-chart
    /*
    var ctx = document.getElementById("test-chart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["City 1", "City 2", "City 3"],
            datasets: [
                {
                    type: 'bar',
                    label: '10th_percentile_wage',
                    data: [12, 19, 3],
                    backgroundColor: 'red',
                    borderColor: 'red',
                    borderWidth: 1
                },
                {
                    type: 'bar',
                    label: '25th_percentile_wage',
                    data: [10, 11, 12],
                    backgroundColor: 'blue',
                    borderColor: 'blue',
                    borderWidth: 1

                },

                {
                    type: 'bar',
                    label: '75th_percentile_wage',
                    data: [10, 11, 12],
                    backgroundColor: 'green',
                    borderColor: 'green',
                    borderWidth: 1
                },
                {
                    type: 'bar',
                    label: '90th_percentile_wage',
                    data: [10, 11, 12],
                    backgroundColor: 'yellow',
                    borderColor: 'yellow',
                    borderWidth: 1
                },

            ]
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                   // stacked: true
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value, index, values) {
                            if(parseInt(value) > 1000){
                                return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                                return '$' + value;
                            }
                        }
                    }
                }]
            }
        }
    });

    */




});