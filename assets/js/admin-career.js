jQuery( document).ready( function( $ ){
   // $( 'body').append( '<div class="career-modal-drop"></div>' );
   // $( 'body').append( '<div class="career-modal"></div>' );
    var $body = $( 'body' );

    // close modal
    $body.on( 'click', '.career-modal-wrapper .media-modal-close', function( e ) {
        e.preventDefault();
        $( this).closest( '.career-modal-wrapper').hide();
    } );


    //  Modal
    $body.on( 'click', '.careerify_career_modal',  function( e ) {
        e.preventDefault();
        var post_id = $( this).attr( 'data-id' ) || '';
        var action = $( this).attr( 'data-action' ) || '';
        if ( $( '#career-modal-schools-id-' + post_id ).length > 0 ) {
            $( '#career-modal-schools-id-' + post_id).show();
        } else {
            $.ajax( {
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'careerify_admin_ajax',
                    doing: action,
                    post_id: post_id,
                    nonce: CareerAdmin.nonce,
                },
                dataType: 'html',
                success: function( res ) {
                    $( '.career-modal-wrapper').hide();
                    $body.append( res );
                    $( '#'+action+'-id-' + post_id).show();
                }
            } );
        }
    } );


    // Sync Salary
    $body.on( 'click', '.career-salary-sheet-input .button', function( e ) {
        e.preventDefault();
        var p = $( this ).closest( '.sheet-input' );
        var input = p.find( 'input.text');
        var button = p.find( '.button');
        var url = p.find( 'input.text').val();
        url = url.trim();
        var action = button.attr( 'data-action' ) || '';

        var patt = new RegExp("docs.google.com/spreadsheets");
        if ( ! url || ! patt.test( url ) ) {
            input.addClass('error');
            button.removeClass( 'updating-message' );
        } else {
            input.removeClass('error');
            button.addClass( 'updating-message' );
            var post_id = input.attr( 'data-post-id' );

            $.ajax( {
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'careerify_admin_ajax',
                    doing: action,
                    google_sheer_url: url,
                    post_id: post_id,
                    nonce: CareerAdmin.nonce,
                },
                dataType: 'html',
                cache: false,
                error: function(){
                    button.removeClass( 'updating-message' );
                },
                success: function( res ) {
                    button.removeClass( 'updating-message' );
                    var get_action = 'get_schools';
                    if ( action == 'sync_schools' ) {
                        // Reload table
                        $( '.careerify_career_schools').html( res );
                    } else if ( action == 'sync_salary_states' ) {
                        get_action = 'get_salary_states';
                    } else {
                        get_action = 'get_salary_cities';
                    }

                    $.ajax( {
                        url: ajaxurl,
                        type: 'GET',
                        data: {
                            action: 'careerify_admin_ajax',
                            doing: get_action,
                            post_id: post_id,
                            nonce: CareerAdmin.nonce,
                        },
                        cache: false,
                        dataType: 'html',
                        success: function( html ) {
                            var wrap = button.closest( '.media-frame-content' );
                            var table = wrap.find( '.ajax-table-data' );
                            html = $( html );
                            table.html( html.find( '.ajax-table-data').html() );
                        }
                    } );

                }
            } );

        }

    } );


    // Ajax table data
    $body.on( 'click', '.ajax-table-data .pagination-links a, .ajax-table-data thead a, .ajax-table-data tfoot a', function( e ) {
        e.preventDefault();
        var url = $( this).attr( 'href' );
        var table = $( this).closest( '.ajax-table-data' );
        $.get( url, function( html ){
            html = $( html );
            table.html( html.find( '.ajax-table-data').html() );
        } );
    } );

    // Unlimited item
    $body.on( 'click', '.unlimited-inputs .new-item', function( e ){
        e.preventDefault();
        var  p = $( this).closest( '.unlimited-inputs' );
        var clone = $( '.sheet-input', p).eq( 0 ) .clone();
        clone.find( 'input[type="text"]').val( '' );
        clone.insertBefore( this );
    } );

    // Remove Unlimited item
    $body.on( 'click', '.unlimited-inputs .remove-item', function( e ){
        e.preventDefault();

        var c = confirm( 'Are you sure ?' );
        if ( c ) {
            var  pp = $( this ).closest( '.unlimited-inputs' );
            var  p = $( this ).closest( '.sheet-input' );

            var program_remove = p.find( '.program-name').val();

            if ( $( '.sheet-input', pp).length > 1 ) {
                p.remove();
            } else {
                p.find( 'input[type="text"]').val( '' );
            }

            var post_id =  $( this).attr( 'data-post-id' );

            $.ajax( { // Sync school table
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'careerify_admin_ajax',
                    doing: 'remove_school_program',
                    post_id: post_id,
                    program: program_remove,
                    nonce: CareerAdmin.nonce,
                },
                cache: false,
                dataType: 'html',
                success: function( html ) {

                    $.ajax( {
                        url: ajaxurl,
                        type: 'GET',
                        data: {
                            action: 'careerify_admin_ajax',
                            doing: 'get_schools',
                            post_id: post_id,
                            nonce: CareerAdmin.nonce,
                        },
                        cache: false,
                        dataType: 'html',
                        success: function( html ) {
                            var wrap = pp.closest( '.media-frame-content' );
                            var table = wrap.find( '.ajax-table-data' );
                            html = $( html );
                            table.html( html.find( '.ajax-table-data').html() );
                        }
                    } );


                }
            } );

        }

    } );



    var schools_claw = function ( respond_data, button ) {
        var _index = 0;
        var post_id =  button.attr( 'data-post-id' );

        var ajax_claw = function(){
            var data;
            try {
                data = respond_data.schools_ids[ _index  ];
                _index ++;
                //$( '.media-frame-title h1').text( _index );
                if ( _index == respond_data.schools_ids.length - 1 ) { // All done
                    button.removeClass( 'updating-message' );
                    button.html( CareerAdmin.sync );

                    $.ajax( {
                        url: ajaxurl,
                        type: 'GET',
                        data: {
                            action: 'careerify_admin_ajax',
                            doing: 'get_schools',
                            post_id: post_id,
                            nonce: CareerAdmin.nonce,
                        },
                        cache: false,
                        dataType: 'html',
                        success: function( html ) {
                            var wrap = button.closest( '.media-frame-content' );
                            var table = wrap.find( '.ajax-table-data' );
                            html = $( html );
                            table.html( html.find( '.ajax-table-data').html() );
                        }
                    } );

                    return ;
                }
            } catch ( e ) {

            }

            if ( ! data ) {
                return ;
            }


            var status = CareerAdmin.sync_status.replace( '%s', ( _index + 1 ) +'/' + respond_data.schools_ids.length );
            button.html( status );


            var url = 'http://nces.ed.gov/collegenavigator/?s=all&p=' + data.program_id + '&id=' + data.IPEDS_ID;
            // var url = 'http://nces.ed.gov/collegenavigator/?s=all&p=51.2306+51.1107&id=177834#programs';
            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'careerify_admin_ajax',
                    doing: 'get_html',
                    url: url,
                    nonce: CareerAdmin.nonce,
                },
                dataType: 'html',
                cache: false,
                error: function () {

                },
                success: function ( res ) {
                    var $html = $( res );
                    $html.find( 'img').remove();
                    var dg = [];
                    var pr = [];
                    $( '.pmtabular thead.mainrow th', $html).each( function(){
                        dg.push( $( this).find( 'a' ).html() );
                       // console.log( $( this).find( 'a' ).html() );
                    } );

                    $( '.pmtabular tbody tr.pmhighlight td', $html).each( function(){
                        pr.push( $( this ).text() );
                    } );

                    $.ajax({
                        url: ajaxurl,
                        type: 'POST',
                        data: {
                            action: 'careerify_admin_ajax',
                            doing: 'insert_program',
                            dg: dg,
                            pr: pr,
                            data: data,
                            //program_name: respond_data.program_name,
                            //program_slug: respond_data.program_slug,
                            career_id: respond_data.career_id,
                            nonce: CareerAdmin.nonce,
                        },
                        dataType: 'html',
                        cache: false,
                        error: function () {

                        },
                        success: function ( res ) {
                            // Call until completed
                            ajax_claw();
                        }
                    });


                }
            });

        };

        ajax_claw();
    };




    $body.on( 'click', '.career-schools-sheet-input .sync',  function( e ){
        e.preventDefault();
        var item  = $( this ).parent();
        var program_name = item.find( 'input.program-name').val();
        var sheet_url = item.find( 'input.program-sheet').val();
        var button = item.find( '.button.sync' );
        button.addClass( 'updating-message' );
        var post_id = button.attr( 'data-post-id' );

        var programs = [];
        var  pp = item.closest( '.unlimited-inputs' );
        pp.find( '.sheet-input').each( function(){
            var _i =  $( this );
            var p = _i.find( 'input.program-name').val();
            if( p ) {
                programs.push( p );
            }

        } );

        // Request ajax to start Sync
        $.ajax({
            url: ajaxurl,
            type: 'GET',
            data: {
                action: 'careerify_admin_ajax',
                doing: 'sync_schools',
                program: program_name,
                programs: programs,
                post_id: post_id,
                sheet: sheet_url,
                nonce: CareerAdmin.nonce,
            },
            dataType: 'json',
            cache: false,
            error: function () {
                button.removeClass('updating-message');
            },
            success: function ( res ) {
                //button.removeClass('updating-message');
                if ( res.success ) {
                    schools_claw( res.data, button );
                }
            }
        });
    } );







    // Career Salary modal
    /*
    $body.on( 'click', '.careerify_career_salary',  function( e ) {
        e.preventDefault();
        var post_id = $( this).attr( 'data-id' ) || '';
        if ( $( '#career-modal-salary-id-' + post_id ).length > 0 ) {
            $( '#career-modal-salary-id-' + post_id).show();
        } else {
            $.ajax( {
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'careerify_admin_ajax',
                    doing: 'get_salary_cites',
                    post_id: post_id,
                    nonce: CareerAdmin.nonce,
                },
                dataType: 'html',
                success: function( res ) {
                    $( '.career-modal-wrapper').hide();
                    $body.append( res );
                    $( '#career-modal-salary-id-' + post_id).show();
                }
            } );
        }
    } );
    */




} ) ;